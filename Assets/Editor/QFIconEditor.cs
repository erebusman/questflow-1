﻿using QuestFlow;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/*
 * Copyright 2015 Carl Kidwell
 *
 * QuestFlow Quest and Dialogue editor for Unity 3D
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class QFIconEditor : EditorWindow
{
    public IconList iconList;
    private int viewIndex = 1;

    [MenuItem("Window/QuestFlow Icon Editor %#a")]
    private static void Init()
    {
        EditorWindow.GetWindow(typeof(QFIconEditor));
    }

    private void OnEnable()
    {
        if (EditorPrefs.HasKey("ObjectPath"))
        {
            string objectPath = EditorPrefs.GetString("ObjectPath");
            iconList = AssetDatabase.LoadAssetAtPath(objectPath, typeof(IconList)) as IconList;
        }
    }

    private void OnGUI()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("QuestFlow Icon Editor", EditorStyles.boldLabel);
        if (iconList != null)
        {
            if (GUILayout.Button("Show Icon List"))
            {
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = iconList;
            }
            if (GUILayout.Button("Open Icon List"))
            {
                OpenItemList();
            }
            if (GUILayout.Button("New Icon List"))
            {
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = iconList;
            }
        }

        GUILayout.EndHorizontal();

        if (iconList == null)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            if (GUILayout.Button("Create New Icon List", GUILayout.ExpandWidth(false)))
            {
                CreateNewIconList();
            }
            if (GUILayout.Button("Open Existing Icon List", GUILayout.ExpandWidth(false)))
            {
                OpenItemList();
            }
            GUILayout.EndHorizontal();
        }

        GUILayout.Space(20);

        if (iconList != null && iconList.iconItemList != null)
        {
            GUILayout.Space(10);
            if (GUILayout.Button("Import SpriteSheet", GUILayout.ExpandWidth(false)))
            {
                importSpriteSheet();
            }
            GUILayout.Label("Imported sprite sheet MUST be in your /Assets/ path!");
            GUILayout.Label("Imported sprites will ALL be of the currently item's selected Item Type");
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Prev", GUILayout.ExpandWidth(false)))
            {
                if (viewIndex > 1)
                {
                    viewIndex--;
                }
            }
            GUILayout.Space(10);
            if (GUILayout.Button("Next", GUILayout.ExpandWidth(false)))
            {
                if (viewIndex < iconList.iconItemList.Count)
                {
                    viewIndex++;
                }
            }

            GUILayout.Space(60);

            if (GUILayout.Button("Add Icon", GUILayout.ExpandWidth(false)))
            {
                AddItem();
            }
            if (GUILayout.Button("Delete Icon", GUILayout.ExpandWidth(false)))
            {
                DeleteItem(viewIndex - 1);
            }

            GUILayout.EndHorizontal();

            if (iconList.iconItemList.Count > 0)
            {
                GUILayout.BeginHorizontal();

                viewIndex = Mathf.Clamp(EditorGUILayout.IntField("Current Icon", viewIndex, GUILayout.ExpandWidth(false)), 1, iconList.iconItemList.Count);
                EditorGUILayout.LabelField("of    " + iconList.iconItemList.Count.ToString() + "   items", "", GUILayout.ExpandWidth(false));
                GUILayout.EndHorizontal();

                iconList.iconItemList[viewIndex - 1].iconID = EditorGUILayout.IntField("Icon ID", iconList.iconItemList[viewIndex - 1].iconID);
                iconList.iconItemList[viewIndex - 1]._type = (IconItem.spriteType)EditorGUILayout.EnumPopup("Item Type", iconList.iconItemList[viewIndex - 1]._type);
                GUILayout.Space(10);
                iconList.iconItemList[viewIndex - 1].icon = EditorGUILayout.ObjectField("Icon Icon", iconList.iconItemList[viewIndex - 1].icon, typeof(Sprite), false) as Sprite;

                GUILayout.Space(10);
            }
            else
            {
                GUILayout.Label("This Icon database is empty");
            }
        }
        else if (iconList != null && iconList.iconItemList == null)
        {
            if (GUILayout.Button("Add your first Icon", GUILayout.ExpandWidth(false)))
            {
                AddItem();
            }
        }

        if (GUI.changed)
        {
            EditorUtility.SetDirty(iconList);
        }
    }

    private void DeleteItem(int index)
    {
        iconList.iconItemList.RemoveAt(index);
        if (iconList.idIndex > 0)
        {
            iconList.idIndex--;
        }
    }

    private void AddItem()
    {
        IconItem newItem = new IconItem();

        if (iconList.iconItemList == null)
        {
            iconList.iconItemList = new List<IconItem>();
        }
        iconList.iconItemList.Add(newItem);
        iconList.idIndex++;
        viewIndex = iconList.iconItemList.Count;
        iconList.iconItemList[viewIndex - 1].iconID = iconList.idIndex;
    }

    private void importSpriteSheet()
    {
        //TODO might want a way to set ALL of the incoming sprites to a particular sprite type e.g. NPC, Sword, Dagger, etc

        var path = EditorUtility.OpenFilePanel("Select a Sprite Sheet to import", "", "");
        if (path.Length != 0)
        {
            IconItem tempItem = new IconItem();
            tempItem._type = iconList.iconItemList[viewIndex - 1]._type;
            int position = path.IndexOf("Assets");

            string truncatedPath = path.Substring(position);

            // NOTE the Unity method LoadAllAssetRepresentationsAtPath is ALWAYS RELATIVE to your /Assets/ path!
            // this is why I am truncating the path to the position of the assets folder above
            Object[] myObject = AssetDatabase.LoadAllAssetRepresentationsAtPath(truncatedPath);

            foreach (Object _obj in myObject)
            {
                AddItem();
                iconList.iconItemList[viewIndex - 1].icon = (Sprite)_obj;
                iconList.iconItemList[viewIndex - 1]._type = tempItem._type;
            }
        }
    }

    private void OpenItemList()
    {
        string absPath = EditorUtility.OpenFilePanel("Select Icon Database", "", "");
        if (absPath.StartsWith(Application.dataPath))
        {
            string relPath = absPath.Substring(Application.dataPath.Length - "Assets".Length);
            iconList = AssetDatabase.LoadAssetAtPath(relPath, typeof(IconList)) as IconList;
            if (iconList)
            {
                EditorPrefs.SetString("ObjectPath", relPath);
            }
        }
    }

    public void CreateNewIconList()
    {
        viewIndex = 1;
        iconList = CreateIconList.Create();
        if (iconList)
        {
            string relPath = AssetDatabase.GetAssetPath(iconList);
            EditorPrefs.SetString("ObjectPath", relPath);
        }
    }
}