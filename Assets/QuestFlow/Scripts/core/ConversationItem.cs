﻿using System.Collections.Generic;

/*
 * Copyright 2015 Carl Kidwell
 *
 * QuestFlow Quest and Dialogue editor for Unity 3D
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace QuestFlow
{
    [System.Serializable]
    public class ConversationItem
    {
        public int conversationID;          // Each conversation has an unique ID
        public bool isRootConversation;     // Some conversations are top level or root conversations, is this one?

        public List<int> npcs;          // NPC's involved in this conversation

        public int locationID;       // where is this taking place

        public int soundToPlayID;       // a sound to play when this dialogue comes up , voice over etc

        public string questEntryText;       // this is the string that is presented in the parent to get you here

        public int parentID;                // if it is not a root conversation what is the parentID
        public int questID;                 // if a parent what quest is this attached to
        public List<int> childIDs;          // what children are under this conversation

        public int unlocksQuestID;            //  is there an quest id this unlocks
        public int unlocksConversationID;     // is there a conversation ID this unlocks
        public int blocksConversationID;        // is there a conversation ID this blocks?

        public bool alreadyShown = false;   // if this item was previously shown (you might grey it out - or not show again)
        public bool locked;               // will only be unlocked if another item unlocks it
        public bool blocked;                // will only be unblocked if another item unblocks it

        public string conversationText;     // the quest dialogue that will appear
    }
}