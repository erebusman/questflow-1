﻿using UnityEngine;

/*
 * Copyright 2015 Carl Kidwell
 *
 * QuestFlow Quest and Dialogue editor for Unity 3D
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#if UNITY_EDITOR

using UnityEditor;

#endif

namespace QuestFlow
{
    [System.Serializable]
    public class CreateIconList
    {
#if UNITY_EDITOR

        [MenuItem("Tools/QuestFlow/Create Icon List")]

        public static IconList Create()
        {
            IconList asset = ScriptableObject.CreateInstance<IconList>();

            AssetDatabase.CreateAsset(asset, "Assets/IconList.asset");
            AssetDatabase.SaveAssets();

            EditorUtility.FocusProjectWindow();

            return asset;
        }

        public static IconList CreateByName(string _name)
        {
            IconList asset = ScriptableObject.CreateInstance<IconList>();

            string assetPath = "Assets/";
            string assetPathName = assetPath + _name + ".asset";

            AssetDatabase.CreateAsset(asset, assetPathName);
            AssetDatabase.SaveAssets();

            return asset;
        }
#endif
    }
}