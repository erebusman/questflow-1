﻿using System.IO;
using UnityEngine;

/*
 * Copyright 2015 Carl Kidwell
 *
 * QuestFlow Quest and Dialogue editor for Unity 3D
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace QuestFlow
{
    public class QFQuestContainer : MonoBehaviour
    {
        public string campaignName;

        public Campaign campaign = new Campaign();
        public QuestList quests = new QuestList();
        public NPCList npcs = new NPCList();
        public LocationItemList locations = new LocationItemList();
        public AchievementList achievements = new AchievementList();
        public ConvoList conversations = new ConvoList();

        public void loadCampaigns()
        {
            campaign = Campaign.Load(Path.Combine(Application.dataPath, campaignName + "_camp.xml"));
            npcs = NPCList.Load(Path.Combine(Application.dataPath, campaignName + "_npcs.xml"));
            achievements = AchievementList.Load(Path.Combine(Application.dataPath, campaignName + "_achiev.xml"));
            conversations = ConvoList.Load(Path.Combine(Application.dataPath, campaignName + "_conv.xml"));
            locations = LocationItemList.Load(Path.Combine(Application.dataPath, campaignName + "_locs.xml"));
            quests = QuestList.Load(Path.Combine(Application.dataPath, campaignName + "_quests.xml"));
        }

        public void saveCampaign()
        {
            campaign.Save(Path.Combine(Application.dataPath, campaignName + "_camp.xml"));
            npcs.Save(Path.Combine(Application.dataPath, campaignName + "_npcs.xml"));
            achievements.Save(Path.Combine(Application.dataPath, campaignName + "_achiev.xml"));
            conversations.Save(Path.Combine(Application.dataPath, campaignName + "_conv.xml"));
            locations.Save(Path.Combine(Application.dataPath, campaignName + "_locs.xml"));
            quests.Save(Path.Combine(Application.dataPath, campaignName + "_quests.xml"));
        }
    }
}