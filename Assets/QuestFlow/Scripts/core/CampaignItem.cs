﻿using UnityEngine;

/*
 * Copyright 2015 Carl Kidwell
 *
 * QuestFlow Quest and Dialogue editor for Unity 3D
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace QuestFlow
{
    [System.Serializable]
    public class CampaignItem
    {
        public string campaignName;         // the name of this campaign
        public string campaignDescription;  // the description of this campaign

        public int campaignID;              // the internal ID of this campaign

        public int campaignIconID;

        public int levelMin;                // the minimum recommend character level for this campaign
        public int levelMax;                // the maximum recommend character level for this campaign

        public Sprite splashGraphic;        // a banner or splash image for the campaign

        public string campaignListName;
        public string npcListName;
        public string questListName;
        public string conversationListName;
        public string locationListName;
        public string achievementListName;
    }
}