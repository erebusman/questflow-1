﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

/*
 * Copyright 2015 Carl Kidwell
 *
 * QuestFlow Quest and Dialogue editor for Unity 3D
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace QuestFlow
{
    [System.Serializable]
    [XmlRoot("Conversations")]
    public class ConvoList
    {
        public int conversationIDIndex;
        public List<ConversationItem> convoList;

        public void Save(string path)
        {
            var serializer = new XmlSerializer(typeof(ConvoList));
            using (var stream = new FileStream(path, FileMode.Create))
            {
                serializer.Serialize(stream, this);
                stream.Close();
            }
        }

        public static ConvoList Load(string path)
        {
            var serializer = new XmlSerializer(typeof(ConvoList));
            using (var stream = new FileStream(path, FileMode.Open))
            {
                return serializer.Deserialize(stream) as ConvoList;
            }
        }

        //Loads the xml directly from the given string. Useful in combination with www.text.
        public static ConvoList LoadFromText(string text)
        {
            var serializer = new XmlSerializer(typeof(ConvoList));
            return serializer.Deserialize(new StringReader(text)) as ConvoList;
        }
    }
}