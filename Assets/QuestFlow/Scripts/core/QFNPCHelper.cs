﻿using System.Linq;

/*
 * Copyright 2015 Carl Kidwell
 *
 * QuestFlow Quest and Dialogue editor for Unity 3D
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace QuestFlow
{
    public class QFNPCHelper
    {
        public static string getNPC_Name_ByID(int npcID, NPCList npcList)
        {
            NPCItem npc = npcList.npcItemList.Where(x => x.npcID == npcID).SingleOrDefault();

            return npc.npcName;
        }

        public static NPCItem getNPCByName(string _npcName, NPCList npcList)
        {
            NPCItem npcToAdd = npcList.npcItemList.Where(x => x.npcName == _npcName).SingleOrDefault();

            return npcToAdd;
        }

        public static int getNPC_ID_ByName(string npcName, NPCList npcList)
        {
            NPCItem npcToAdd = npcList.npcItemList.Where(x => x.npcName == npcName).SingleOrDefault();

            return npcToAdd.npcID;
        }
    }
}