﻿using System.Collections.Generic;
using UnityEngine;

/*
 * Copyright 2015 Carl Kidwell
 *
 * QuestFlow Quest and Dialogue editor for Unity 3D
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace QuestFlow
{
    public class QuestFlowEventManager : MonoBehaviour
    {
        private static QuestFlowEventManager _instance;

        public static QuestFlowEventManager Instance

        {
            get
            {
                if (_instance != null)
                {
                    return _instance;
                }
                _instance = (QuestFlowEventManager)GameObject.FindObjectOfType(typeof(QuestFlowEventManager));
                if (_instance == null && Application.isPlaying)
                {
                    Debug.LogError("ALERT!!! : No Quest Flow Event Manager prefab in this Scene. Calls to this will fail.");
                }

                return _instance;
            }

            set
            {
                _instance = null;
            }
        }

        // A List of QuestEvent observers
        private List<Component> questEventObservers = new List<Component>();

        public void AddObserver(Component observer)
        {
            AddObserver(observer, null);
        }

        public void AddObserver(Component observer, object sender)
        {
            if (!questEventObservers.Contains(observer)) { questEventObservers.Add(observer); }
        }

        public void RemoveObserver(Component observer)
        {
            if (questEventObservers.Contains(observer)) { questEventObservers.Remove(observer); }
        }

        public void PostQuestEvent(Component sender, QuestEvent _event)
        {
            // checking for invalid observers to remove later to avoid problems
            // observers may have de-registered or game objects gone inactive , etc
            List<Component> observersToRemove = new List<Component>();

            foreach (Component observer in questEventObservers)
            {
                if (!observer)
                {
                    observersToRemove.Add(observer);
                }
                else
                {
                    // If the observer is valid, then send it the notification.
                    observer.SendMessage("questevent", _event, SendMessageOptions.DontRequireReceiver);
                }
            }

            // Remove any invalid observers
            foreach (Component observer in observersToRemove)
            {
                questEventObservers.Remove(observer);
            }
        }
    }

    public class QuestEvent
    {
        public int eventID;

        public eventType myEvent;

        public enum eventType
        {
            QUEST,
            CONVERSATION,
            ACHIEVEMENT,
            NPC,
            LOCATION
        }
    }
}