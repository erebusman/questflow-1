﻿using System.Linq;

/*
 * Copyright 2015 Carl Kidwell
 *
 * QuestFlow Quest and Dialogue editor for Unity 3D
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace QuestFlow
{
    public class QFConvoHelper
    {
        public static int conversationNameToID(string _name, ConvoList _list)
        {
            ConversationItem convo = _list.convoList.Where(x => x.questEntryText == _name).SingleOrDefault();

            return convo.conversationID;
        }

        public static ConversationItem getConvoByID(int _id, ConvoList _list)
        {
            return _list.convoList.Where(x => x.conversationID == _id).SingleOrDefault();
        }
    }
}