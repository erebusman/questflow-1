﻿using QuestFlow;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/*
 * Copyright 2015 Carl Kidwell
 *
 * QuestFlow Quest and Dialogue editor for Unity 3D
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class QFNPCController : MonoBehaviour
{
    private static int NPC_ID_INDEX;

    public InputField npcName;
    public InputField npcDescription;
    public InputField iconPickerPageNumberInput;

    public Sprite npcIcon;
    public Image npcImage;
    public Image currentNPCImage;
    public GameObject npcScrollPanel;
    public GameObject chooseNPCPopUpPanel;
    public IconList iconList;
    private Image[] npcIcons = new Image[12];
    private int currentIconPage = 1;
    private int currentNPCIconPage = 1;

    public bool iconPickerList = false;
    public int indexOficons = 0;
    public int indexofNPCS = 0;

    private List<Sprite> iconSprites = new List<Sprite>();

    private NPCItem tempNPC = new NPCItem();

    private NPCList npcList;

    private QFClickableNPC[] clickableNPCs = new QFClickableNPC[12];

    // Use this for initialization
    private void Start()
    {
        npcList = gameObject.GetComponent<QFMenuController>().npcs;
        NPC_ID_INDEX = npcList.npc_id_index;
        iconList = gameObject.GetComponent<QFMenuController>().npcIconList;
    }

    private void OnEnable()
    {
        npcList = gameObject.GetComponent<QFMenuController>().npcs;

        NPC_ID_INDEX = npcList.npc_id_index;
    }

    public void tabLoaded()
    {
        chooseNPCPopUpPanel.SetActive(false);
        if (npcIcons[0] == null)
        {
            initNPCScrollPanel();
        }
        npcListChanged(false);
    }

    public bool npcNameDupeCheck(string checkName)
    {
        List<string> npcNames = new List<string>();

        foreach (NPCItem npc in npcList.npcItemList)
        {
            npcNames.Add(npc.npcName);
        }

        return npcNames.Contains(checkName);
    }

    public void saveNewNPC()
    {
        npcList = gameObject.GetComponent<QFMenuController>().npcs;

        // name dupe check here
        if (npcNameDupeCheck(tempNPC.npcName))
        {
            gameObject.GetComponent<QFMenuController>().postAlert("Duplicate NPC Name Error", "Duplicate NPC name detected. This NPC has not been saved as duplicate names are not permitted");
        }
        else
        {
            tempNPC.npcName = npcName.text;
            tempNPC.description = npcDescription.text;

            bool idAssigned = false;
            int idToUse = npcList.npc_id_index++;

            if (npcList.npcItemList == null)
            {
                npcList.npcItemList = new List<NPCItem>();
            }

            while (!idAssigned)
            {
                NPCItem testForDupeID = npcList.npcItemList.Where(x => x.npcID == idToUse).SingleOrDefault();
                if (testForDupeID == null)
                {
                    idAssigned = true;
                    tempNPC.npcID = idToUse;
                }
                else
                {
                    idToUse = npcList.npc_id_index++;
                }
            }

            npcList.npcItemList.Add(tempNPC);
            npcListChanged(false);
        }
    }

    public void saveNPC()
    {
        npcList = gameObject.GetComponent<QFMenuController>().npcs;

        bool found = false;
        foreach (NPCItem npc in npcList.npcItemList)
        {
            if (npc == tempNPC)
            {
                npc.npcName = npcName.text;
                npc.description = npcDescription.text;
                found = true;
            }
        }

        if (!found)
        {
            saveNewNPC();
        }

        npcListChanged(false);
    }

    public void deleteNPC()
    {
        npcList.npcItemList.Remove(tempNPC);
        // calling create NPC as it clears the currentyl selected tempNPC and GUi elements
        createNPC();
    }

    public void createNPC()
    {
        if (npcList == null)
        {
            npcList = gameObject.GetComponent<QFMenuController>().npcs;
        }

        tempNPC = new NPCItem();

        tempNPC.npcName = "";
        tempNPC.description = "";

        npcName.text = "";
        npcDescription.text = "";

        npcIcon = new Sprite();

        npcListChanged(false);
    }

    public void npcListChanged(bool iconPicker)
    {
        // public bool iconPickerList = false;
        //public int indexOficons = 0;
        //public int indexofNPCS = 0;

        npcList = gameObject.GetComponent<QFMenuController>().npcs;

        if (iconPicker)
        {
            iconPickerList = true;
            int iconCount = 0;

            if (iconList.iconItemList.Count < 12)
            {
                iconCount = iconList.iconItemList.Count;
            }
            else
            {
                iconCount = 12;
            }

            int x = 0;
            int indexcount = 0;

            foreach (IconItem _icon in iconList.iconItemList)
            {
                if (indexcount >= indexOficons)
                {
                    if (x < iconCount)
                    {
                        npcIcons[x].sprite = getIconByID(_icon.iconID, npcIcons[x].sprite);
                        clickableNPCs[x].npc = null;
                        clickableNPCs[x].iconPickerActive = true;
                        clickableNPCs[x].iconNumber = _icon.iconID;
                    }
                    if (x >= iconCount && x < 12)
                    {
                        clickableNPCs[x].npc = null;
                        clickableNPCs[x].iconPickerActive = true;
                        clickableNPCs[x].iconNumber = 0;
                    }
                    x++;
                }

                indexcount++;
            }
        }
        else
        {
            iconPickerList = false;
            int iconCount = 0;
            if (npcList.npcItemList.Count < 12)
            {
                iconCount = npcList.npcItemList.Count;
            }
            else
            {
                iconCount = 12;
            }
            int x = 0;
            int indexcount = 0;
            foreach (NPCItem npc in npcList.npcItemList)
            {
                if (indexcount >= indexOficons)
                {
                    if (x < iconCount)
                    {
                        npcIcons[x].sprite = getIconByID(npc.npcIconID, npcIcons[x].sprite);
                        clickableNPCs[x].npc = npc;
                        clickableNPCs[x].selected = false;
                        clickableNPCs[x].updateSelectedColor();
                        clickableNPCs[x].iconPickerActive = false;
                    }
                    if (x >= iconCount && x < 12)
                    {
                        clickableNPCs[x].npc = null;
                        clickableNPCs[x].iconPickerActive = false;
                        npcIcons[x].sprite = getIconByID(0, npcIcons[x].sprite);
                    }
                    x++;
                }

                indexcount++;
            }
            if (x < 12)
            {
                for (int y = x; x < 12; x++)
                {
                    clickableNPCs[x].npc = null;
                    clickableNPCs[x].iconPickerActive = false;
                    npcIcons[x].sprite = getIconByID(0, npcIcons[x].sprite);
                }
            }
        }
    }

    public void npcSelected(NPCItem _selectedNPC)
    {
        NPCItem thisDude = npcList.npcItemList.Where(x => x.npcID == _selectedNPC.npcID).SingleOrDefault();

        tempNPC = thisDude;
        currentNPCImage.sprite = getIconByID(thisDude.npcIconID, currentNPCImage.sprite);
        npcName.text = thisDude.npcName;
        npcDescription.text = thisDude.description;

        resetIconSelectionHighlight();
    }

    public void resetIconSelectionHighlight()
    {
        int iconCount = 0;
        if (npcList.npcItemList.Count < 12)
        {
            iconCount = npcList.npcItemList.Count;
        }
        else
        {
            iconCount = 12;
        }
        int x = 0;
        foreach (NPCItem npc in npcList.npcItemList)
        {
            if (x < iconCount)
            {
                npcIcons[x].GetComponent<QFClickableNPC>().selected = false;
                npcIcons[x].GetComponent<QFClickableNPC>().updateSelectedColor();
            }
            x++;
        }
    }

    public void assignIcon(int iconNumber)
    {
        tempNPC.npcIconID = iconNumber;

        currentNPCImage.sprite = getIconByID(tempNPC.npcIconID, currentNPCImage.sprite);
        resetIconSelectionHighlight();
    }

    public Sprite getIconByID(int _id, Sprite _sprite)
    {
        foreach (IconItem _icon in iconList.iconItemList)
        {
            if (_icon.iconID == _id)
            {
                _sprite = _icon.icon;
            }
        }

        return _sprite;
    }

    public void pageTabs(bool forward)
    {
        int desiredPage;

        if (iconPickerList)
        {
            desiredPage = currentNPCIconPage;
        }
        else
        {
            desiredPage = currentIconPage;
        }

        if (forward)
        {
            desiredPage++;
        }
        else
        {
            desiredPage--;
        }
        pageTabs(desiredPage);
    }

    public void pageTabsByGUI()
    {
        int desiredPage;

        try
        {
            desiredPage = Int32.Parse(iconPickerPageNumberInput.text);
        }
        catch
        {
            desiredPage = 1;
        }

        if (desiredPage < 1)
        {
            desiredPage = 1;
        }

        pageTabs(desiredPage);
    }

    public void pageTabs(int page)
    {
        int iconCount;
        if (iconPickerList)
        {
            iconCount = iconList.iconItemList.Count;
        }
        else
        {
            iconCount = npcList.npcItemList.Count;
        }

        int totalPageCount = iconCount / 12;

        if (totalPageCount <= 0)
        {
            totalPageCount = 1;
        }

        int targetPage = 1;

        if (page > totalPageCount)
        {
            targetPage = totalPageCount;
            indexOficons = iconCount - 12;
        }
        else if (page < 1)
        {
            targetPage = 1;
            indexOficons = 0;
        }
        else
        {
            targetPage = page;
            indexOficons = targetPage * 12 - 12;
        }

        if (iconPickerList)
        {
            currentNPCIconPage = targetPage;
            npcListChanged(true);
        }
        else
        {
            currentIconPage = targetPage;
            npcListChanged(false);
        }
    }

    private void initNPCScrollPanel()
    {
        npcIcons[0] = npcImage;
        clickableNPCs[0] = npcImage.GetComponent<QFClickableNPC>();

        for (int x = 1; x < 12; x++)
        {
            Image newButton = (Image)Instantiate(npcImage, transform.position, transform.rotation);
            newButton.transform.SetParent(npcScrollPanel.transform, true);
            npcIcons[x] = newButton.transform.GetComponent<Image>();
            newButton.name = "npcImage" + (x);
            newButton.GetComponent<QFClickableNPC>().npc = null;
            newButton.GetComponent<QFClickableNPC>().iconNumber = x;
            clickableNPCs[x] = newButton.GetComponent<QFClickableNPC>();
        }
    }
}