﻿using QuestFlow;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/*
 * Copyright 2015 Carl Kidwell
 *
 * QuestFlow Quest and Dialogue editor for Unity 3D
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class QFConversationController : MonoBehaviour

{
    private static int NUMBER_OF_CONVS = 15;
    private int indexOfParents = 0;
    private int indexOfChildren = 0;
    private int currentParentPage = 1;
    private int currentChildpage = 1;

    private ConversationItem tempConversation = new ConversationItem();
    private ConvoList convoList;
    private NPCList npcList;

    public InputField titleInput;
    public InputField conversationTextInput;

    public Dropdown parentConvoDropdown;
    public Dropdown parentQuestDropdown;
    public Dropdown questUnlocksDropdown;
    public Dropdown conversationUnlocksDropdown;
    public Dropdown conversationBlocksDropdown;
    public Dropdown availableNPCsDropdown;
    public Dropdown npcsInConvoDropdown;
    public Dropdown locationDropdown;

    public Toggle lockedToggle;
    public Toggle blockedToggle;

    public GameObject mainTab;
    public GameObject extendedOptionsTab;
    public GameObject leftNavConvoItem;
    public GameObject rightNavConvoItem;
    public GameObject[] leftNavs;
    public GameObject[] rightNavs;
    public GameObject leftNavPanel;
    public GameObject rightNavPanel;

    public InputField parentPageInput;
    public InputField childPageInput;

    private bool firstTimeLoadDone = false;

    private void Start()
    {
        convoList = gameObject.GetComponent<QFMenuController>().conversations;
        npcList = gameObject.GetComponent<QFMenuController>().npcs;
    }

    public void tabLoaded()
    {
        if (convoList == null)
        {
            convoList = gameObject.GetComponent<QFMenuController>().conversations;
        }

        if (npcList == null)
        {
            npcList = gameObject.GetComponent<QFMenuController>().npcs;
        }

        if (!firstTimeLoadDone)
        {
            initNavPanels();
            firstTimeLoadDone = true;
        }
        indexOfParents = 0;
        indexOfChildren = 0;

        mainTab.SetActive(true);
        extendedOptionsTab.SetActive(false);
        displayAllTopLevelConversations();
        availableNPCDropDown();
    }

    public void selectParentConvo()
    {
        tempConversation.parentID = QFConvoHelper.conversationNameToID(parentConvoDropdown.captionText.text, convoList);
        tempConversation.isRootConversation = false;
        displayAllTopLevelConversations();

        conversationSelected(QFConvoHelper.getConvoByID(tempConversation.parentID, convoList), true);
    }

    public void selectParentQuest()
    {
        //TODO
    }

    public void selectQuestThisUnlocks()
    {
        //TODO
    }

    public void selectConvoThisUnlocks()
    {
        tempConversation.unlocksConversationID = QFConvoHelper.conversationNameToID(conversationUnlocksDropdown.captionText.text, convoList);
    }

    public void selectConvoThisBlocks()
    {
        tempConversation.blocksConversationID = QFConvoHelper.conversationNameToID(conversationBlocksDropdown.captionText.text, convoList);
    }

    public void toggleLocked()
    {
        tempConversation.locked = lockedToggle.isOn;
    }

    public void toggleBlocked()
    {
        tempConversation.blocked = blockedToggle.isOn;
    }

    public void initNewConvo()
    {
        mainTab.SetActive(true);
        extendedOptionsTab.SetActive(true);

        availableNPCDropDown();
        npcsInThisConversationDropDown();
        selectParentDropdownContents();
        initQuestDropdownContents(parentQuestDropdown);
        initQuestDropdownContents(questUnlocksDropdown);
        initConvoDropdowns(conversationUnlocksDropdown);
        initConvoDropdowns(conversationBlocksDropdown);

        titleInput.text = tempConversation.questEntryText;
        conversationTextInput.text = tempConversation.conversationText;

        lockedToggle.isOn = tempConversation.locked;
        blockedToggle.isOn = tempConversation.blocked;

        mainTab.SetActive(true);
        extendedOptionsTab.SetActive(false);
    }

    public void saveNewConversation()
    {
        convoList = gameObject.GetComponent<QFMenuController>().conversations;

        tempConversation.questEntryText = titleInput.text;
        tempConversation.conversationText = conversationTextInput.text;

        bool idAssigned = false;
        int idToUse = convoList.conversationIDIndex++;

        if (convoList.convoList == null)
        {
            convoList.convoList = new List<ConversationItem>();
        }

        while (!idAssigned)
        {
            ConversationItem testForDupeID = convoList.convoList.Where(x => x.conversationID == idToUse).SingleOrDefault();
            if (testForDupeID == null)
            {
                idAssigned = true;
                tempConversation.conversationID = idToUse;
            }
            else
            {
                idToUse = convoList.conversationIDIndex++;
            }
        }

        convoList.convoList.Add(tempConversation);
        displayAllTopLevelConversations();
    }

    public void saveConversation()
    {
        convoList = gameObject.GetComponent<QFMenuController>().conversations;

        bool found = false;
        foreach (ConversationItem conv in convoList.convoList)
        {
            if (conv == tempConversation)
            {
                conv.questEntryText = tempConversation.questEntryText;
                conv.conversationText = tempConversation.conversationText;
                conv.conversationID = tempConversation.conversationID;
                conv.isRootConversation = tempConversation.isRootConversation;
                conv.parentID = tempConversation.parentID;
                conv.questID = tempConversation.questID;

                conv.locationID = tempConversation.locationID;
                conv.npcs = tempConversation.npcs;
                conv.childIDs = tempConversation.childIDs;

                conv.soundToPlayID = tempConversation.soundToPlayID;
                conv.unlocksConversationID = tempConversation.unlocksConversationID;
                conv.unlocksQuestID = tempConversation.unlocksQuestID;
                conv.blocksConversationID = tempConversation.blocksConversationID;

                conv.alreadyShown = tempConversation.alreadyShown;
                conv.locked = tempConversation.locked;
                conv.blocked = tempConversation.blocked;

                found = true;
            }
        }

        if (!found)
        {
            saveNewConversation();
        }
        else
        {
            displayAllTopLevelConversations();
        }
    }

    public void deleteConversation()
    {
        convoList.convoList.Remove(tempConversation);
        // calling create NPC as it clears the currentyl selected tempNPC and GUi elements
        createConversation();
    }

    public void createConversation()
    {
        if (convoList == null)
        {
            convoList = gameObject.GetComponent<QFMenuController>().conversations;
        }

        tempConversation = new ConversationItem();

        tempConversation.questEntryText = "A new Top Level Quest";
        tempConversation.conversationText = "";

        tempConversation.isRootConversation = true;
        tempConversation.parentID = -1;
        tempConversation.questID = -1;

        tempConversation.locationID = -1;
        tempConversation.npcs = new List<int>();
        tempConversation.childIDs = new List<int>();
        //TODO need to add soundtoplay to GUI
        tempConversation.soundToPlayID = 0;
        tempConversation.unlocksConversationID = -1;
        tempConversation.unlocksQuestID = -1;
        tempConversation.blocksConversationID = -1;

        tempConversation.alreadyShown = false;
        tempConversation.locked = false;
        tempConversation.blocked = false;

        displayAllTopLevelConversations();
        initNewConvo();
    }

    public void conversationSelected(ConversationItem _conversation, bool parent)
    {
        convoList = gameObject.GetComponent<QFMenuController>().conversations;

        if (_conversation != null)
        {
            ConversationItem _conv = convoList.convoList.Where(x => x.conversationID == _conversation.conversationID).SingleOrDefault();

            tempConversation = _conv;

            displayChildrenOf(tempConversation);

            if (tempConversation != null)
            {
                initNewConvo();
            }
        }
    }

    public void selectParentDropdownContents()
    {
        bool firstset = false;
        parentConvoDropdown.options.Clear();

        int count = 0;
        foreach (ConversationItem _conv in convoList.convoList)
        {
            if (_conv.isRootConversation)
            {
                count++;
                Dropdown.OptionData item = new Dropdown.OptionData();
                item.text = _conv.questEntryText;

                if (!firstset)
                {
                    parentConvoDropdown.captionText.text = _conv.questEntryText;
                    firstset = true;
                }
                parentConvoDropdown.options.Add(item);
            }
        }

        if (count == 0)
        {
            parentConvoDropdown.captionText.text = "No Parents Found";
        }
    }

    public void initConvoDropdowns(Dropdown _dropdown)
    {
        bool firstset = false;
        _dropdown.options.Clear();

        int count = 0;
        foreach (ConversationItem _conv in convoList.convoList)
        {
            count++;
            Dropdown.OptionData item = new Dropdown.OptionData();
            item.text = _conv.questEntryText;

            if (!firstset)
            {
                _dropdown.captionText.text = _conv.questEntryText;
                firstset = true;
            }
            _dropdown.options.Add(item);
        }

        if (count == 0)
        {
            _dropdown.captionText.text = "NONE FOUND";
        }
    }

    public void initQuestDropdownContents(Dropdown _dropdown)
    {
        QuestList questList = gameObject.GetComponent<QFMenuController>().quests;

        bool firstset = false;
        _dropdown.options.Clear();

        int count = 0;
        foreach (QuestItem _quest in questList.questList)
        {
            count++;
            Dropdown.OptionData item = new Dropdown.OptionData();
            item.text = _quest.questName;

            if (!firstset)
            {
                _dropdown.captionText.text = _quest.questName;
                firstset = true;
            }
            _dropdown.options.Add(item);
        }

        if (count == 0)
        {
            _dropdown.captionText.text = "No Quests Found";
        }
    }

    public void displayChildrenOf(ConversationItem _parent)
    {
        convoList = gameObject.GetComponent<QFMenuController>().conversations;

        int desiredCount = 0;

        if (!_parent.isRootConversation)
        {
            displayOnlyThisParent(_parent);
        }

        foreach (ConversationItem _conv in convoList.convoList)
        {
            if (desiredCount < NUMBER_OF_CONVS && _conv.parentID == _parent.conversationID)
            {
                rightNavs[desiredCount].GetComponent<QFClickableConversation>().conversation = _conv;
                rightNavs[desiredCount].GetComponentInChildren<Text>().text = _conv.questEntryText;
                desiredCount++;
            }
        }

        if (desiredCount < NUMBER_OF_CONVS - 1)
        {
            blankChildren(desiredCount);
        }
    }

    public void blankChildren(int startingIndex)
    {
        for (int i = startingIndex; i < 15; i++)
        {
            rightNavs[i].GetComponentInChildren<Text>().text = "";
            rightNavs[i].GetComponentInChildren<QFClickableConversation>().conversation = null;
        }
    }

    public void blankParents(int startingIndex)
    {
        for (int i = startingIndex; i < 15; i++)
        {
            leftNavs[i].GetComponentInChildren<Text>().text = "";
            leftNavs[i].GetComponentInChildren<QFClickableConversation>().conversation = null;
        }
    }

    public void displayOnlyThisParent(ConversationItem _parent)
    {
        convoList = gameObject.GetComponent<QFMenuController>().conversations;

        currentParentPage = 1;

        foreach (ConversationItem _conv in convoList.convoList)
        {
            if (_conv == _parent)
            {
                leftNavs[0].GetComponent<QFClickableConversation>().conversation = _conv;
                leftNavs[0].GetComponentInChildren<Text>().text = _conv.questEntryText;
            }
        }

        blankParents(1);
    }

    public void displayAllTopLevelConversations()
    {
        convoList = gameObject.GetComponent<QFMenuController>().conversations;

        currentParentPage = 1;
        int desiredCount = 0;

        foreach (ConversationItem _conv in convoList.convoList)
        {
            if (desiredCount < NUMBER_OF_CONVS && _conv.isRootConversation)
            {
                leftNavs[desiredCount].GetComponent<QFClickableConversation>().conversation = _conv;
                leftNavs[desiredCount].GetComponentInChildren<Text>().text = _conv.questEntryText;
                desiredCount++;
            }
        }

        if (desiredCount < NUMBER_OF_CONVS)
        {
            for (int i = desiredCount; i < NUMBER_OF_CONVS; i++)
            {
                leftNavs[desiredCount].GetComponent<QFClickableConversation>().conversation = null;
                leftNavs[desiredCount].GetComponentInChildren<Text>().text = "";
            }
        }
    }

    public void pageTabsByGUI(bool leftSide)
    {
        int desiredPage;

        try
        {
            if (leftSide)
            {
                desiredPage = Int32.Parse(parentPageInput.text);
            }
            else
            {
                desiredPage = Int32.Parse(childPageInput.text);
            }
        }
        catch
        {
            desiredPage = 1;
        }

        if (desiredPage < 1)
        {
            desiredPage = 1;
        }

        pageTabs(desiredPage, leftSide);
    }

    public void pageTabForwardLeft(bool forward)
    {
        pageTabs(forward, true);
    }

    public void pageTabForwardRight(bool forward)
    {
        pageTabs(forward, false);
    }

    public void pageTabs(bool forward, bool leftSide)
    {
        int desiredPage = 0;
        if (leftSide)
        {
            desiredPage = currentParentPage;
        }
        else
        {
            desiredPage = currentChildpage;
        }

        if (forward)
        {
            desiredPage++;
        }
        else
        {
            desiredPage--;
        }
        pageTabs(desiredPage, leftSide);
    }

    public void pageTabs(int page, bool leftSide)
    {
        int iconCount = convoList.convoList.Count;

        int totalPageCount = iconCount / NUMBER_OF_CONVS;

        if (totalPageCount <= 0)
        {
            totalPageCount = 1;
        }

        int targetPage = 1;

        if (page > totalPageCount)
        {
            targetPage = totalPageCount;
            if (leftSide)
            {
                indexOfParents = iconCount - NUMBER_OF_CONVS;
            }
            else
            {
                indexOfChildren = iconCount - NUMBER_OF_CONVS;
            }
        }
        else if (page < 1)
        {
            targetPage = 1;

            if (leftSide)
            {
                indexOfParents = 0;
            }
            else
            {
                indexOfChildren = 0;
            }
        }
        else
        {
            targetPage = page;
            if (leftSide)
            {
                indexOfParents = targetPage * NUMBER_OF_CONVS - NUMBER_OF_CONVS;
            }
            else
            {
                indexOfChildren = targetPage * NUMBER_OF_CONVS - NUMBER_OF_CONVS;
            }
        }

        if (leftSide)
        {
            currentParentPage = targetPage;
        }
        else
        {
            currentChildpage = targetPage;
        }

        //locationIconsChanged();
    }

    public void availableNPCDropDown()
    {
        npcList = gameObject.GetComponent<QFMenuController>().npcs;

        bool firstset = false;
        availableNPCsDropdown.options.Clear();

        foreach (NPCItem _npc in npcList.npcItemList)
        {
            Dropdown.OptionData item = new Dropdown.OptionData();
            item.text = _npc.npcName;

            if (!firstset)
            {
                availableNPCsDropdown.captionText.text = _npc.npcName;
                firstset = true;
            }
            availableNPCsDropdown.options.Add(item);
        }
    }

    public void addCurrentNPCtoConversation()
    {
        tempConversation.npcs.Add(QFNPCHelper.getNPC_ID_ByName(availableNPCsDropdown.captionText.text, npcList));

        npcsInThisConversationDropDown();
    }

    public void removeCurrentNPCfromConversation()
    {
        tempConversation.npcs.Remove(QFNPCHelper.getNPC_ID_ByName(npcsInConvoDropdown.captionText.text, npcList));
        npcsInThisConversationDropDown();
    }

    public void npcsInThisConversationDropDown()
    {
        bool firstset = false;
        npcsInConvoDropdown.options.Clear();

        int count = 0;

        foreach (int _npcID in tempConversation.npcs)
        {
            Dropdown.OptionData item = new Dropdown.OptionData();
            item.text = QFNPCHelper.getNPC_Name_ByID(_npcID, npcList);
            count++;
            if (!firstset)
            {
                npcsInConvoDropdown.captionText.text = QFNPCHelper.getNPC_Name_ByID(_npcID, npcList);
                firstset = true;
            }
            npcsInConvoDropdown.options.Add(item);
        }

        if (count == 0)
        {
            npcsInConvoDropdown.captionText.text = "NONE";
        }
    }

    private void initNavPanels()
    {
        leftNavs = new GameObject[15];
        rightNavs = new GameObject[15];

        leftNavs[0] = leftNavConvoItem;
        leftNavs[0].GetComponentInChildren<Text>().text = "";
        leftNavs[0].GetComponentInChildren<QFClickableConversation>().conversation = null;

        rightNavs[0] = rightNavConvoItem;
        rightNavs[0].GetComponentInChildren<Text>().text = "";
        rightNavs[0].GetComponentInChildren<QFClickableConversation>().conversation = null;

        for (int i = 1; i < 15; i++)
        {
            GameObject lNavGo = Instantiate(leftNavConvoItem);
            lNavGo.transform.SetParent(leftNavPanel.transform);
            leftNavs[i] = lNavGo;
            leftNavs[i].GetComponentInChildren<Text>().text = "";
            leftNavs[i].GetComponentInChildren<QFClickableConversation>().conversation = null;

            GameObject rNavGo = Instantiate(rightNavConvoItem);
            rNavGo.transform.SetParent(rightNavPanel.transform);
            rightNavs[i] = rNavGo;
            rightNavs[i].GetComponentInChildren<Text>().text = "";
            rightNavs[i].GetComponentInChildren<QFClickableConversation>().conversation = null;
        }
    }
}