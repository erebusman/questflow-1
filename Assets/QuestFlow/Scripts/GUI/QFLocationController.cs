﻿using QuestFlow;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/*
 * Copyright 2015 Carl Kidwell
 *
 * QuestFlow Quest and Dialogue editor for Unity 3D
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class QFLocationController : MonoBehaviour
{
    private static int NUMBER_OF_ICONS = 10;
    private static int NUMBER_OF_LOCATIONS = 6;

    public LocationItemList locations;
    public NPCList npcList;

    public InputField locationNameInput;
    public InputField descriptionInput;

    public Dropdown availableNPCs;
    public Dropdown npcsInThisLocation;

    public Image locBackGroundImage;

    public IconList iconList;
    private Image[] backgroundIcons = new Image[NUMBER_OF_ICONS];
    private Image[] locationPanels = new Image[NUMBER_OF_LOCATIONS];
    public Image backgroundIcon00;
    public GameObject scrollPanel;
    public GameObject locationScrollPanel;
    public Image locationPanelImage;

    private LocationItem tempLocation = new LocationItem();

    private QFClickableBackgrounds[] clickableBGs = new QFClickableBackgrounds[NUMBER_OF_ICONS];
    private QFClickableLocation[] clickableLocs = new QFClickableLocation[NUMBER_OF_LOCATIONS];

    private int currentIconPage = 1;
    public InputField iconPickerPageNumberInput;
    public int indexOficons = 0;
    private int currentLocPage = 1;
    public int indexOfLocicons = 0;

    private bool firstTimeInitDone = false;

    // Use this for initialization
    private void Start()
    {
        iconList = gameObject.GetComponent<QFMenuController>().backgroundIconList;
        locations = gameObject.GetComponent<QFMenuController>().locations;
        npcList = gameObject.GetComponent<QFMenuController>().npcs;
    }

    public void tabLoaded()
    {
        if (iconList == null)
        {
            iconList = gameObject.GetComponent<QFMenuController>().backgroundIconList;
        }

        if (locations == null)
        {
            locations = gameObject.GetComponent<QFMenuController>().locations;
        }

        if (!firstTimeInitDone)
        {
            initBackgroundScrollPanel();
            firstTimeInitDone = true;
        }

        locationIconsChanged();
        locationsChanged();
    }

    public void availableNPCDropDown()
    {
        npcList = gameObject.GetComponent<QFMenuController>().npcs;

        bool firstset = false;
        availableNPCs.options.Clear();

        foreach (NPCItem _npc in npcList.npcItemList)
        {
            Dropdown.OptionData item = new Dropdown.OptionData();
            item.text = _npc.npcName;

            if (!firstset)
            {
                availableNPCs.captionText.text = _npc.npcName;
                firstset = true;
            }
            availableNPCs.options.Add(item);
        }
    }

    public void addCurrentNPCtoLocation()
    {
        tempLocation.npcs.Add(QFNPCHelper.getNPC_ID_ByName(availableNPCs.captionText.text, npcList));

        npcsInThisLocationDropDown();
    }

    public NPCItem getNPCByName(string _npcName)
    {
        npcList = gameObject.GetComponent<QFMenuController>().npcs;

        NPCItem npcToAdd = npcList.npcItemList.Where(x => x.npcName == _npcName).SingleOrDefault();

        return npcToAdd;
    }

    public void removeCurrentNPCfromLocation()
    {
        tempLocation.npcs.Remove(QFNPCHelper.getNPC_ID_ByName(npcsInThisLocation.captionText.text, npcList));
        npcsInThisLocationDropDown();
    }

    public void npcsInThisLocationDropDown()
    {
        bool firstset = false;
        npcsInThisLocation.options.Clear();

        int count = 0;

        foreach (int _npcID in tempLocation.npcs)
        {
            Dropdown.OptionData item = new Dropdown.OptionData();
            item.text = QFNPCHelper.getNPC_Name_ByID(_npcID, npcList);
            count++;
            if (!firstset)
            {
                npcsInThisLocation.captionText.text = QFNPCHelper.getNPC_Name_ByID(_npcID, npcList);
                firstset = true;
            }
            npcsInThisLocation.options.Add(item);
        }

        if (count == 0)
        {
            npcsInThisLocation.captionText.text = "NONE";
        }
    }

    public void updateBGIcon(int iconNumber)
    {
        tempLocation.backdropGraphicID = iconNumber;

        locBackGroundImage.sprite = getIconByID(tempLocation.backdropGraphicID, locBackGroundImage.sprite);
    }

    public void saveNewLocation()
    {
        locations = gameObject.GetComponent<QFMenuController>().locations;

        tempLocation.locationName = locationNameInput.text;
        tempLocation.description = descriptionInput.text;

        bool idAssigned = false;
        int idToUse = locations.locationIDIndex++;

        if (locations.locationList == null)
        {
            locations.locationList = new List<LocationItem>();
        }

        while (!idAssigned)
        {
            LocationItem testForDupeID = locations.locationList.Where(x => x.locationID == idToUse).SingleOrDefault();
            if (testForDupeID == null)
            {
                idAssigned = true;
                tempLocation.locationID = idToUse;
            }
            else
            {
                idToUse = locations.locationIDIndex++;
            }
        }

        locations.locationList.Add(tempLocation);
        locationsChanged();
    }

    public void saveLocation()
    {
        locations = gameObject.GetComponent<QFMenuController>().locations;

        bool found = false;
        foreach (LocationItem loc in locations.locationList)
        {
            if (loc == tempLocation)
            {
                loc.locationName = locationNameInput.text;
                loc.description = descriptionInput.text;
                loc.backdropGraphicID = tempLocation.backdropGraphicID;
                found = true;
            }
        }

        if (!found)
        {
            saveNewLocation();
        }
        else
        {
            locationsChanged();
        }
    }

    public void deleteLocation()
    {
        locations.locationList.Remove(tempLocation);
        // calling create NPC as it clears the currentyl selected tempNPC and GUi elements
        createLocation();
    }

    public void createLocation()
    {
        if (locations == null)
        {
            locations = gameObject.GetComponent<QFMenuController>().locations;
        }

        tempLocation = new LocationItem();

        tempLocation.locationName = "";
        tempLocation.description = "";

        locationNameInput.text = "";
        descriptionInput.text = "";

        locBackGroundImage.sprite = new Sprite();

        locationsChanged();
    }

    public void locationSelected(LocationItem _location)
    {
        if (_location != null)
        {
            LocationItem _loc = locations.locationList.Where(x => x.locationID == _location.locationID).SingleOrDefault();

            tempLocation = _loc;
            if (tempLocation != null)
            {
                locBackGroundImage.sprite = getIconByID(tempLocation.backdropGraphicID, locBackGroundImage.sprite);
                locationNameInput.text = tempLocation.locationName;
                descriptionInput.text = tempLocation.description;
            }
            availableNPCDropDown();
            npcsInThisLocationDropDown();
        }

        //resetIconSelectionHighlight();
    }

    public Sprite getIconByID(int _id, Sprite _sprite)
    {
        iconList = gameObject.GetComponent<QFMenuController>().backgroundIconList;

        foreach (IconItem _icon in iconList.iconItemList)
        {
            if (_icon.iconID == _id)
            {
                _sprite = _icon.icon;
            }
        }

        return _sprite;
    }

    public void locationIconsChanged()
    {
        iconList = gameObject.GetComponent<QFMenuController>().backgroundIconList;

        int iconCount = 0;

        if (iconList.iconItemList.Count < NUMBER_OF_ICONS)
        {
            iconCount = iconList.iconItemList.Count;
        }
        else
        {
            iconCount = NUMBER_OF_ICONS;
        }

        int x = 0;
        int indexcount = 0;

        foreach (IconItem _icon in iconList.iconItemList)
        {
            if (indexcount >= indexOficons)
            {
                if (x < iconCount)
                {
                    backgroundIcons[x].sprite = getIconByID(_icon.iconID, backgroundIcons[x].sprite);
                    clickableBGs[x].iconNumber = _icon.iconID;
                }
                if (x >= iconCount && x < NUMBER_OF_ICONS)
                {
                    clickableBGs[x].iconNumber = 0;
                }
                x++;
            }

            indexcount++;
        }
    }

    public void pageTabs(bool forward)
    {
        int desiredPage = currentIconPage;

        if (forward)
        {
            desiredPage++;
        }
        else
        {
            desiredPage--;
        }
        pageTabs(desiredPage);
    }

    public void pageTabsByGUI()
    {
        int desiredPage;

        try
        {
            desiredPage = Int32.Parse(iconPickerPageNumberInput.text);
        }
        catch
        {
            desiredPage = 1;
        }

        if (desiredPage < 1)
        {
            desiredPage = 1;
        }

        pageTabs(desiredPage);
    }

    public void pageTabs(int page)
    {
        int iconCount = iconList.iconItemList.Count;

        int totalPageCount = iconCount / NUMBER_OF_ICONS;

        if (totalPageCount <= 0)
        {
            totalPageCount = 1;
        }

        int targetPage = 1;

        if (page > totalPageCount)
        {
            targetPage = totalPageCount;
            indexOficons = iconCount - NUMBER_OF_ICONS;
        }
        else if (page < 1)
        {
            targetPage = 1;
            indexOficons = 0;
        }
        else
        {
            targetPage = page;
            indexOficons = targetPage * 10 - NUMBER_OF_ICONS;
        }

        currentIconPage = targetPage;
        locationIconsChanged();
    }

    public void locPageTabs(bool forward)
    {
        int desiredPage = currentLocPage;

        if (forward)
        {
            desiredPage++;
        }
        else
        {
            desiredPage--;
        }
        locPageTabs(desiredPage);
    }

    public void locPageTabsByGUI()
    {
        int desiredPage;

        try
        {
            desiredPage = Int32.Parse(iconPickerPageNumberInput.text);
        }
        catch
        {
            desiredPage = 1;
        }

        if (desiredPage < 1)
        {
            desiredPage = 1;
        }

        locPageTabs(desiredPage);
    }

    public void locPageTabs(int page)
    {
        int iconCount = locations.locationList.Count;

        int totalPageCount = iconCount / NUMBER_OF_LOCATIONS;

        if (totalPageCount <= 0)
        {
            totalPageCount = 1;
        }

        int targetPage = 1;

        if (page > totalPageCount)
        {
            targetPage = totalPageCount;
            indexOfLocicons = iconCount - NUMBER_OF_LOCATIONS;
        }
        else if (page < 1)
        {
            targetPage = 1;
            indexOfLocicons = 0;
        }
        else
        {
            targetPage = page;
            indexOfLocicons = targetPage * 10 - NUMBER_OF_LOCATIONS;
        }

        currentLocPage = targetPage;
        locationsChanged();
    }

    public void locationsChanged()
    {
        locations = gameObject.GetComponent<QFMenuController>().locations;

        int iconCount = 0;

        if (locations.locationList.Count < NUMBER_OF_LOCATIONS)
        {
            iconCount = locations.locationList.Count;
        }
        else
        {
            iconCount = NUMBER_OF_LOCATIONS;
        }

        int x = 0;
        int indexcount = 0;

        foreach (LocationItem _loc in locations.locationList)
        {
            if (indexcount >= indexOficons)
            {
                if (x < iconCount)
                {
                    locationPanels[x].GetComponentInChildren<Text>().text = _loc.locationName;
                    clickableLocs[x].locationItem = _loc;
                }

                if (x >= iconCount && x < NUMBER_OF_LOCATIONS)
                {
                    clickableLocs[x].locationItem = null;
                    locationPanels[x].GetComponentInChildren<Text>().text = "EMPTY";
                }
                x++;
            }
            indexcount++;
        }

        if (x < NUMBER_OF_LOCATIONS)
        {
            for (int y = x; y < NUMBER_OF_LOCATIONS; y++)
            {
                clickableLocs[y].locationItem = null;
                locationPanels[y].GetComponentInChildren<Text>().text = "EMPTY";
            }
        }
    }

    private void initBackgroundScrollPanel()
    {
        backgroundIcons[0] = backgroundIcon00;
        clickableBGs[0] = backgroundIcon00.GetComponent<QFClickableBackgrounds>();

        for (int x = 1; x < NUMBER_OF_ICONS; x++)
        {
            Image newButton = (Image)Instantiate(backgroundIcon00, transform.position, transform.rotation);
            newButton.transform.SetParent(scrollPanel.transform, true);
            backgroundIcons[x] = newButton.transform.GetComponent<Image>();
            newButton.name = "image" + "0" + (x);
            clickableBGs[x] = newButton.GetComponent<QFClickableBackgrounds>();
            clickableBGs[x].iconNumber = x;
        }

        locationPanels[0] = locationPanelImage;
        locationPanels[0].GetComponentInChildren<Text>().text = "EMPTY";
        clickableLocs[0] = locationPanelImage.GetComponent<QFClickableLocation>();

        for (int i = 1; i < NUMBER_OF_LOCATIONS; i++)
        {
            Image newPanel = (Image)Instantiate(locationPanelImage, transform.position, transform.rotation);
            newPanel.transform.SetParent(locationScrollPanel.transform, true);
            newPanel.name = "locPanelItem_" + "0" + (i);

            locationPanels[i] = newPanel.transform.GetComponent<Image>();
            locationPanels[i].GetComponentInChildren<Text>().text = "EMPTY";

            clickableLocs[i] = newPanel.GetComponent<QFClickableLocation>();
            clickableLocs[i].locationItem = null;
        }
    }
}