﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

/*
 * Copyright 2015 Carl Kidwell
 *
 * QuestFlow Quest and Dialogue editor for Unity 3D
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace QuestFlow
{
    public class QFMenuController : MonoBehaviour
    {
        public Dropdown campaignDropDown;

        public IconList npcIconList;
        public IconList backgroundIconList;
        public IconList achievementIconList;

        public InputField campNameInput;
        public InputField campDescInput;
        public InputField minLevel;
        public InputField maxLevel;

        public Campaign campaign = new Campaign();
        public QuestList quests = new QuestList();
        public NPCList npcs = new NPCList();
        public LocationItemList locations = new LocationItemList();
        public AchievementList achievements = new AchievementList();
        public ConvoList conversations = new ConvoList();

        public GameObject campaignTab;
        public GameObject NPCTab;
        public GameObject QuestTab;
        public GameObject ConversationTab;
        public GameObject AchievementTab;
        public GameObject LocationTab;

        public GameObject alertBox;
        public Text alertTitle;
        public Text alertBody;

        private List<Campaign> campaignsOnDisk = new List<Campaign>();

        // Use this for initialization
        private void Start()
        {
            setInitTabStates();
            loadCampaignDropDown();
        }

        public void loadCampaignDropDown()
        {
            bool firstset = false;
            campaignDropDown.options.Clear();
            List<string> campaignList = Campaign.listCampaignsFromDisk();

            foreach (string _campaign in campaignList)
            {
                Dropdown.OptionData item = new Dropdown.OptionData();
                item.text = _campaign;
                campaignsOnDisk.Add(Campaign.Load(Path.Combine(Application.dataPath, _campaign)));

                if (!firstset)
                {
                    campaignDropDown.captionText.text = _campaign;
                    firstset = true;
                }
                campaignDropDown.options.Add(item);
            }
        }

        public void loadActiveCampaignByDropDown()
        {
            campaign = Campaign.Load(Path.Combine(Application.dataPath, campaignDropDown.captionText.text.ToString()));

            npcs = NPCList.Load(Path.Combine(Application.dataPath, campaign.campaign.npcListName + ".xml"));
            achievements = AchievementList.Load(Path.Combine(Application.dataPath, campaign.campaign.achievementListName + ".xml"));
            conversations = ConvoList.Load(Path.Combine(Application.dataPath, campaign.campaign.conversationListName + ".xml"));
            locations = LocationItemList.Load(Path.Combine(Application.dataPath, campaign.campaign.locationListName + ".xml"));
            quests = QuestList.Load(Path.Combine(Application.dataPath, campaign.campaign.questListName + ".xml"));
        }

        public void loadCampaigns()
        {
            campaign = Campaign.Load(Path.Combine(Application.dataPath, "questytest_camp.xml"));
            npcs = NPCList.Load(Path.Combine(Application.dataPath, "questytest_npcs.xml"));
            achievements = AchievementList.Load(Path.Combine(Application.dataPath, "questytest_achiev.xml"));
            conversations = ConvoList.Load(Path.Combine(Application.dataPath, "questytest_conv.xml"));
            locations = LocationItemList.Load(Path.Combine(Application.dataPath, "questytest_locs.xml"));
            quests = QuestList.Load(Path.Combine(Application.dataPath, "questytest_quests.xml"));
        }

        public void saveCampaign()
        {
            campaign.Save(Path.Combine(Application.dataPath, campaign.campaign.campaignName + "_camp.xml"));
            npcs.Save(Path.Combine(Application.dataPath, campaign.campaign.npcListName + ".xml"));
            achievements.Save(Path.Combine(Application.dataPath, campaign.campaign.achievementListName + ".xml"));
            conversations.Save(Path.Combine(Application.dataPath, campaign.campaign.conversationListName + ".xml"));
            locations.Save(Path.Combine(Application.dataPath, campaign.campaign.locationListName + ".xml"));
            quests.Save(Path.Combine(Application.dataPath, campaign.campaign.questListName + ".xml"));
        }

        public void postAlert(String _title, String _body)
        {
            alertBox.SetActive(true);
            alertTitle.text = _title;
            alertBody.text = _body;
        }

        public void showTab(int tabNum)
        {
            // 1 campaign
            // 2 npc
            // 3 quests
            // 4 conversations
            // 5 achievements
            // 6 locations

            if (tabNum == 1)
            {
                campaignTab.SetActive(true);
                NPCTab.SetActive(false);
                QuestTab.SetActive(false);
                ConversationTab.SetActive(false);
                AchievementTab.SetActive(false);
                LocationTab.SetActive(false);
            }
            else if (tabNum == 2)
            {
                campaignTab.SetActive(false);
                NPCTab.SetActive(true);
                QuestTab.SetActive(false);
                ConversationTab.SetActive(false);
                AchievementTab.SetActive(false);
                LocationTab.SetActive(false);
            }
            else if (tabNum == 3)
            {
                campaignTab.SetActive(false);
                NPCTab.SetActive(false);
                QuestTab.SetActive(true);
                ConversationTab.SetActive(false);
                AchievementTab.SetActive(false);
                LocationTab.SetActive(false);
            }
            else if (tabNum == 4)
            {
                campaignTab.SetActive(false);
                NPCTab.SetActive(false);
                QuestTab.SetActive(false);
                ConversationTab.SetActive(true);
                AchievementTab.SetActive(false);
                LocationTab.SetActive(false);
            }
            else if (tabNum == 5)
            {
                campaignTab.SetActive(false);
                NPCTab.SetActive(false);
                QuestTab.SetActive(false);
                ConversationTab.SetActive(false);
                AchievementTab.SetActive(true);
                LocationTab.SetActive(false);
            }
            else if (tabNum == 6)
            {
                campaignTab.SetActive(false);
                NPCTab.SetActive(false);
                QuestTab.SetActive(false);
                ConversationTab.SetActive(false);
                AchievementTab.SetActive(false);
                LocationTab.SetActive(true);
            }
        }

        public bool meetsNamingCriteria(string _value)
        {
            bool meetsAllCriteria = true;
            int test;
            var regexItem = new Regex("^[a-zA-Z0-9 ]*$");

            try
            {
                test = Int32.Parse(campNameInput.text.Substring(0, 1));
                meetsAllCriteria = false;
            }
            catch
            {
            }

            if (!regexItem.IsMatch(campNameInput.text))
            {
                meetsAllCriteria = false;
            }

            return meetsAllCriteria;
        }

        public void CreateCampaign()
        {
            campaign = new Campaign();

            campaign.campaign = new CampaignItem();

            if (campNameInput.text.Length < 1 || !meetsNamingCriteria(campNameInput.text))
            {
                postAlert("Campaign Name Error", "The campaign name must not be blank and must start with an alphabetical character. No special characters are allowed.");
            }
            else
            {
                campaign.campaign.campaignName = campNameInput.text;
            }

            campaign.campaign.campaignDescription = campDescInput.text;

            try
            {
                campaign.campaign.levelMin = Int32.Parse(minLevel.text);
            }
            catch (FormatException e)
            {
                campaign.campaign.levelMin = 0;
                postAlert("Minimum Level Error", "The minimum level must be a number. This field can not be blank. Also no special characters or text allowed!");
                Debug.Log(" **** ERRROR SETTING Campaign levelMin ; not a number! *****");
            }

            try
            {
                campaign.campaign.levelMax = Int32.Parse(maxLevel.text);
            }
            catch
            {
                campaign.campaign.levelMax = 0;
                postAlert("Maximum Level Error", "The maximum level must be a number. This field can not be blank. Also no special characters or text allowed!");
                Debug.Log(" **** ERRROR SETTING Campaign levelMax ; not a number! *****");
            }

            campaign.campaign.campaignListName = campNameInput.text + "_camp";
            campaign.campaign.locationListName = campNameInput.text + "_locs";
            campaign.campaign.npcListName = campNameInput.text + "_npcs";
            campaign.campaign.questListName = campNameInput.text + "_quests";
            campaign.campaign.achievementListName = campNameInput.text + "_achiev";
            campaign.campaign.conversationListName = campNameInput.text + "_conv";

            quests = new QuestList();
            quests.questList = new List<QuestItem>();
            npcs = new NPCList();
            npcs.npcItemList = new List<NPCItem>();
            locations = new LocationItemList();
            locations.locationList = new List<LocationItem>();
            achievements = new AchievementList();
            achievements.achievements = new List<AchievementItem>();
            conversations = new ConvoList();
            conversations.convoList = new List<ConversationItem>();
        }

        public void setInitTabStates()
        {
            campaignTab.SetActive(true);
            NPCTab.SetActive(false);
            QuestTab.SetActive(false);
            ConversationTab.SetActive(false);
            AchievementTab.SetActive(false);
            LocationTab.SetActive(false);
        }
    }
}