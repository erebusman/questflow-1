﻿using QuestFlow;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/*
 * Copyright 2015 Carl Kidwell
 *
 * QuestFlow Quest and Dialogue editor for Unity 3D
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class QFQuestController : MonoBehaviour
{
    private QuestList questList;

    private static int NUMBER_OF_QUESTS = 10;
    private int indexOfParents = 0;
    private int indexOfChildren = 0;
    private int currentParentPage = 1;
    private int currentChildpage = 1;

    public GameObject parentQuestPanel;
    public GameObject childQuestPanel;
    public GameObject leftNavQuestItem;
    public GameObject rightNavQuestItem;
    public GameObject[] leftNavs;
    public GameObject[] rightNavs;

    public InputField questName;
    public InputField questText;
    public Dropdown parentQuestDropDown;
    public Toggle addToJournal;
    public Toggle showOnCompletion;
    public InputField experienceReward;
    public InputField monetaryReward;
    public Toggle proceduralReward;
    public Image questIcon;
    public Dropdown unlockedByThisQuestDropDown;
    public Dropdown allAvailableQuestsDropDown;

    public QuestItem tempQuest = new QuestItem();
    public IconList iconList;
    private IconPanelHelper iconPanelHelper;

    private bool firstTimeInitDone = false;

    // Use this for initialization
    private void Start()
    {
        questList = gameObject.GetComponent<QFMenuController>().quests;
        QFIconEventManager.Instance.AddObserver(this);
        iconList = gameObject.GetComponent<QFMenuController>().npcIconList;
        iconPanelHelper = gameObject.GetComponent<IconPanelHelper>();
    }

    public void tabLoaded()
    {
        if (!firstTimeInitDone)
        {
            initScrollPanels();
            firstTimeInitDone = true;
        }

        createQuest();
        displayAllTopLevelQuests();
        initParentQuestDropDown(parentQuestDropDown);
        availableQuestsDropDown();
        questsUnlockedByCurrentQuestDropDown();
    }

    public void questsUnlockedByCurrentQuestDropDown()
    {
        bool firstset = false;
        unlockedByThisQuestDropDown.options.Clear();

        int count = 0;

        foreach (int _questID in tempQuest.unlocksQuests)
        {
            Dropdown.OptionData item = new Dropdown.OptionData();
            item.text = getQuest_Name_ByID(_questID);
            count++;
            if (!firstset)
            {
                unlockedByThisQuestDropDown.captionText.text = getQuest_Name_ByID(_questID);
                firstset = true;
            }
            unlockedByThisQuestDropDown.options.Add(item);
        }

        if (count == 0)
        {
            unlockedByThisQuestDropDown.captionText.text = "NONE";
        }
    }

    public void addCurrentQuestToUnlockedQuests()
    {
        tempQuest.unlocksQuests.Add(getQuest_ID_ByName(allAvailableQuestsDropDown.captionText.text));
        questsUnlockedByCurrentQuestDropDown();
    }

    public string getQuest_Name_ByID(int _id)
    {
        questList = gameObject.GetComponent<QFMenuController>().quests;

        QuestItem questToAdd = questList.questList.Where(x => x.questID == _id).SingleOrDefault();

        return questToAdd.questName;
    }

    public int getQuest_ID_ByName(string _questName)
    {
        questList = gameObject.GetComponent<QFMenuController>().quests;

        QuestItem questToAdd = questList.questList.Where(x => x.questName == _questName).SingleOrDefault();

        return questToAdd.questID;
    }

    public QuestItem getQuestByName(string _questName)
    {
        questList = gameObject.GetComponent<QFMenuController>().quests;

        QuestItem questToAdd = questList.questList.Where(x => x.questName == _questName).SingleOrDefault();

        return questToAdd;
    }

    public void availableQuestsDropDown()
    {
        questList = gameObject.GetComponent<QFMenuController>().quests;

        bool firstset = false;
        allAvailableQuestsDropDown.options.Clear();

        foreach (QuestItem _quest in questList.questList)
        {
            Dropdown.OptionData item = new Dropdown.OptionData();
            item.text = _quest.questName;

            if (!firstset)
            {
                allAvailableQuestsDropDown.captionText.text = _quest.questName;
                firstset = true;
            }
            allAvailableQuestsDropDown.options.Add(item);
        }
    }

    public void displayAllTopLevelQuests()
    {
        blankChildren(0);
        blankParents(0);

        questList = gameObject.GetComponent<QFMenuController>().quests;

        currentParentPage = 1;
        int desiredCount = 0;

        foreach (QuestItem _quest in questList.questList)
        {
            if (desiredCount < NUMBER_OF_QUESTS && _quest.isRootQuest)
            {
                leftNavs[desiredCount].GetComponent<QFClickableQuest>().questItem = _quest;
                leftNavs[desiredCount].GetComponentInChildren<Text>().text = _quest.questName;
                desiredCount++;
            }
        }

        if (desiredCount < NUMBER_OF_QUESTS)
        {
            for (int i = desiredCount; i < NUMBER_OF_QUESTS; i++)
            {
                leftNavs[desiredCount].GetComponent<QFClickableQuest>().questItem = null;
                leftNavs[desiredCount].GetComponentInChildren<Text>().text = "";
            }
        }
    }

    public void deleteQuest()
    {
        questList.questList.Remove(tempQuest);
        createQuest();
    }

    public void createQuest()
    {
        tempQuest = new QuestItem();
        tempQuest.isRootQuest = true;
        tempQuest.questName = "Quest Name Here";
        tempQuest.questText = "Quest Body here";
        tempQuest.showOnCompletion = true;
        tempQuest.cashReward = 0;
        tempQuest.experienceReward = 0;
        tempQuest.proceduralReward = true;
        tempQuest.addToJournal = true;
        //tempQuest.childQuestIDs = new List<int>();
        tempQuest.unlocksQuests = new List<int>();
        tempQuest.parentQuestID = -1;

        initSelectedQuest();
    }

    public void saveQuest()
    {
        bool found = false;
        foreach (QuestItem _quest in questList.questList)
        {
            if (_quest == tempQuest)
            {
                _quest.questName = tempQuest.questName;
                _quest.questText = tempQuest.questText;
                _quest.addToJournal = tempQuest.addToJournal;
                _quest.showOnCompletion = tempQuest.showOnCompletion;
                _quest.experienceReward = tempQuest.experienceReward;
                _quest.cashReward = tempQuest.cashReward;
                _quest.proceduralReward = tempQuest.proceduralReward;
                _quest.parentQuestID = -1;
                _quest.unlocksQuests = tempQuest.unlocksQuests;
                found = true;
            }
        }

        if (!found)
        {
            saveNewQuest();
        }
        else
        {
            initSelectedQuest();
        }
    }

    public void saveNewQuest()
    {
        tempQuest.questName = questName.text;
        tempQuest.questText = questText.text;

        bool idAssigned = false;
        int idToUse = questList.questIDIndex++;

        if (questList.questList == null)
        {
            questList.questList = new List<QuestItem>();
        }

        while (!idAssigned)
        {
            QuestItem testForDupeID = questList.questList.Where(x => x.questID == idToUse).SingleOrDefault();
            if (testForDupeID == null)
            {
                idAssigned = true;
                tempQuest.questID = idToUse;
            }
            else
            {
                idToUse = questList.questIDIndex++;
            }
        }

        questList.questList.Add(tempQuest);
        initSelectedQuest();
        displayAllTopLevelQuests();
    }

    public void setExperienceReward()
    {
        tempQuest.experienceReward = Int32.Parse(experienceReward.text);
    }

    public void setMonetaryReward()
    {
        tempQuest.cashReward = Int32.Parse(monetaryReward.text);
    }

    public void initSelectedQuest()
    {
        questName.text = tempQuest.questName;
        questText.text = tempQuest.questText;
        addToJournal.isOn = tempQuest.addToJournal;
        showOnCompletion.isOn = tempQuest.showOnCompletion;
        experienceReward.text = tempQuest.experienceReward.ToString();
        monetaryReward.text = tempQuest.cashReward.ToString();
        proceduralReward.isOn = tempQuest.proceduralReward;
        initParentQuestDropDown(parentQuestDropDown);

        availableQuestsDropDown();
        questsUnlockedByCurrentQuestDropDown();
    }

    public void removeParentQuest()
    {
        tempQuest.parentQuestID = -1;
        tempQuest.isRootQuest = true;
        displayAllTopLevelQuests();
    }

    public void setParentQuest()
    {
        questList = gameObject.GetComponent<QFMenuController>().quests;

        QuestItem parent = getQuestByName(parentQuestDropDown.captionText.text);

        tempQuest.parentQuestID = parent.questID;
        tempQuest.isRootQuest = false;

        displayAllTopLevelQuests();

        selectQuest(parent);
    }

    public void initParentQuestDropDown(Dropdown _dropdown)
    {
        bool firstset = false;
        _dropdown.options.Clear();

        int count = 0;
        foreach (QuestItem _quest in questList.questList)
        {
            count++;
            Dropdown.OptionData item = new Dropdown.OptionData();
            item.text = _quest.questName;

            if (!firstset)
            {
                _dropdown.captionText.text = _quest.questName;
                firstset = true;
            }
            _dropdown.options.Add(item);
        }

        if (count == 0)
        {
            _dropdown.captionText.text = "NONE FOUND";
        }
    }

    public void selectQuest(QuestItem _quest)
    {
        questList = gameObject.GetComponent<QFMenuController>().quests;

        if (_quest != null)
        {
            QuestItem _selectedQuest = questList.questList.Where(x => x.questID == _quest.questID).SingleOrDefault();

            tempQuest = _selectedQuest;

            displayChildrenOf(tempQuest);

            if (tempQuest != null)
            {
                initSelectedQuest();
            }
        }
    }

    public void blankChildren(int startingIndex)
    {
        for (int i = startingIndex; i < NUMBER_OF_QUESTS; i++)
        {
            rightNavs[i].GetComponentInChildren<Text>().text = "";
            rightNavs[i].GetComponentInChildren<QFClickableQuest>().questItem = null;
        }
    }

    public void blankParents(int startingIndex)
    {
        for (int i = startingIndex; i < NUMBER_OF_QUESTS; i++)
        {
            leftNavs[i].GetComponentInChildren<Text>().text = "";
            leftNavs[i].GetComponentInChildren<QFClickableQuest>().questItem = null;
        }
    }

    public void displayOnlyThisParent(QuestItem _parent)
    {
        questList = gameObject.GetComponent<QFMenuController>().quests;

        currentParentPage = 1;

        foreach (QuestItem _quest in questList.questList)
        {
            if (_quest == _parent)
            {
                leftNavs[0].GetComponent<QFClickableQuest>().questItem = _quest;
                leftNavs[0].GetComponentInChildren<Text>().text = _quest.questName;
            }
        }

        blankParents(1);
    }

    public void displayChildrenOf(QuestItem _quest)
    {
        questList = gameObject.GetComponent<QFMenuController>().quests;

        int desiredCount = 0;

        if (!_quest.isRootQuest)
        {
            displayOnlyThisParent(_quest);
        }

        foreach (QuestItem _q in questList.questList)
        {
            if (desiredCount < NUMBER_OF_QUESTS && _q.parentQuestID == _quest.questID)
            {
                rightNavs[desiredCount].GetComponent<QFClickableQuest>().questItem = _q;
                rightNavs[desiredCount].GetComponentInChildren<Text>().text = _q.questName;
                desiredCount++;
            }
        }

        if (desiredCount < NUMBER_OF_QUESTS - 1)
        {
            blankChildren(desiredCount);
        }
    }

    public Sprite getIconByID(int _id, Sprite _sprite)
    {
        foreach (IconItem _icon in iconList.iconItemList)
        {
            if (_icon.iconID == _id)
            {
                _sprite = _icon.icon;
            }
        }

        return _sprite;
    }

    public void loadIconPicker()
    {
        iconPanelHelper.loadNPCIcons();
    }

    public void iconEvent(IconEvent _event)
    {
        if (_event.iconType == IconEvent.IconType.NPC)
        {
            tempQuest.npcIconID = _event.iconID;
            questIcon.sprite = getIconByID(tempQuest.npcIconID, questIcon.sprite);
        }
    }

    private void initScrollPanels()
    {
        leftNavs = new GameObject[NUMBER_OF_QUESTS];
        rightNavs = new GameObject[NUMBER_OF_QUESTS];

        leftNavs[0] = leftNavQuestItem;
        leftNavs[0].GetComponentInChildren<Text>().text = "";
        leftNavs[0].GetComponentInChildren<QFClickableQuest>().questItem = null;

        rightNavs[0] = rightNavQuestItem;
        rightNavs[0].GetComponentInChildren<Text>().text = "";
        rightNavs[0].GetComponentInChildren<QFClickableQuest>().questItem = null;

        for (int i = 1; i < NUMBER_OF_QUESTS; i++)
        {
            GameObject lNavGo = Instantiate(leftNavQuestItem);
            lNavGo.transform.SetParent(parentQuestPanel.transform);
            leftNavs[i] = lNavGo;
            leftNavs[i].GetComponentInChildren<Text>().text = "";
            leftNavs[i].GetComponentInChildren<QFClickableQuest>().questItem = null;

            GameObject rNavGo = Instantiate(rightNavQuestItem);
            rNavGo.transform.SetParent(childQuestPanel.transform);
            rightNavs[i] = rNavGo;
            rightNavs[i].GetComponentInChildren<Text>().text = "";
            rightNavs[i].GetComponentInChildren<QFClickableQuest>().questItem = null;
        }
    }
}