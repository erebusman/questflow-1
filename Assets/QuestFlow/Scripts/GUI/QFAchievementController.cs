﻿using QuestFlow;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/*
 * Copyright 2015 Carl Kidwell
 *
 * QuestFlow Quest and Dialogue editor for Unity 3D
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class QFAchievementController : MonoBehaviour
{
    private static int NUMBER_OF_ACHV_ICONS = 10;
    private static int NUMBER_OF_ACHIEVEMENTS = 10;
    private int indexOfParents = 0;
    private int indexOfChildren = 0;
    private int currentParentPage = 1;
    private int currentChildpage = 1;

    public GameObject leftNavAchievItem;
    public GameObject rightNavAchievItem;
    public GameObject[] leftNavs;
    public GameObject[] rightNavs;
    public GameObject leftNavPanel;
    public GameObject rightNavPanel;

    private AchievementList achievements;
    public AchievementItem tempAchievement = new AchievementItem();

    public InputField achievementTitle;
    public InputField achievementDescription;
    public Dropdown achievementParentDropdown;

    public Image currentAchievementIcon;
    public Image achievementIcon;
    public IconList iconList;
    public InputField iconPickerPageNumberInput;
    private Image[] achievementIcons = new Image[NUMBER_OF_ACHV_ICONS];
    private int currentIconPage = 1;
    private int indexOficons = 0;
    public GameObject achievementIconPanel;

    private bool firstTimeInitDone = false;

    // Use this for initialization
    private void Start()
    {
        achievements = gameObject.GetComponent<QFMenuController>().achievements;
        iconList = gameObject.GetComponent<QFMenuController>().achievementIconList;
    }

    // Update is called once per frame
    private void Update()
    {
    }

    public void tabLoaded()
    {
        if (!firstTimeInitDone)
        {
            initScrollPanels();
            firstTimeInitDone = true;
        }
        achievements = gameObject.GetComponent<QFMenuController>().achievements;
        achievementIconsChanged();
        displayAllTopLevelAchievements();
        initParentAchievementDropDown(achievementParentDropdown);
    }

    public void saveAchievement()
    {
        bool found = false;
        foreach (AchievementItem _achievement in achievements.achievements)
        {
            if (_achievement == tempAchievement)
            {
                _achievement.achievementName = tempAchievement.achievementName;
                _achievement.achievementDescription = tempAchievement.achievementDescription;
                _achievement.achievementIconID = tempAchievement.achievementIconID;
                _achievement.isTopLevel = tempAchievement.isTopLevel;
                _achievement.achievementParentID = tempAchievement.achievementParentID;
                _achievement.achievementIconID = tempAchievement.achievementIconID;
                found = true;
            }
        }

        if (!found)
        {
            saveNewAchievement();
        }
        else
        {
            initSelectedAchievement();
        }
    }

    public void deleteAchievement()
    {
        achievements.achievements.Remove(tempAchievement);
        createAchievement();
    }

    public void saveNewAchievement()
    {
        tempAchievement.achievementName = achievementTitle.text;
        tempAchievement.achievementDescription = achievementDescription.text;

        bool idAssigned = false;
        int idToUse = achievements.achivementIDIndex++;

        if (achievements.achievements == null)
        {
            achievements.achievements = new List<AchievementItem>();
        }

        while (!idAssigned)
        {
            AchievementItem testForDupeID = achievements.achievements.Where(x => x.achievementID == idToUse).SingleOrDefault();
            if (testForDupeID == null)
            {
                idAssigned = true;
                tempAchievement.achievementID = idToUse;
            }
            else
            {
                idToUse = achievements.achivementIDIndex++;
            }
        }

        achievements.achievements.Add(tempAchievement);
        initSelectedAchievement();
        displayAllTopLevelAchievements();
    }

    public void createAchievement()
    {
        if (achievements == null)
        {
            achievements = gameObject.GetComponent<QFMenuController>().achievements;
        }

        tempAchievement = new AchievementItem();

        tempAchievement.achievementName = "New Achievement";
        tempAchievement.achievementDescription = "";
        tempAchievement.achievementIconID = 0;
        tempAchievement.isTopLevel = true;
        tempAchievement.achievementParentID = -1;
        currentAchievementIcon.sprite = getIconByID(0, currentAchievementIcon.sprite);
        initSelectedAchievement();
        displayAllTopLevelAchievements();
    }

    public void initSelectedAchievement()
    {
        achievementTitle.text = tempAchievement.achievementName;
        achievementDescription.text = tempAchievement.achievementDescription;
        currentAchievementIcon.sprite = getIconByID(tempAchievement.achievementIconID, currentAchievementIcon.sprite);

        initParentAchievementDropDown(achievementParentDropdown);
    }

    public void achievementSelected(AchievementItem _achievement, bool parent)
    {
        achievements = gameObject.GetComponent<QFMenuController>().achievements;

        if (_achievement != null)
        {
            AchievementItem _achieve = achievements.achievements.Where(x => x.achievementID == _achievement.achievementID).SingleOrDefault();

            tempAchievement = _achieve;

            displayChildrenOf(tempAchievement);

            if (tempAchievement != null)
            {
                initSelectedAchievement();
            }
        }
    }

    public void displayChildrenOf(AchievementItem _parent)
    {
        achievements = gameObject.GetComponent<QFMenuController>().achievements;

        int desiredCount = 0;

        if (!_parent.isTopLevel)
        {
            displayOnlyThisParent(_parent);
        }

        foreach (AchievementItem _achieve in achievements.achievements)
        {
            if (desiredCount < NUMBER_OF_ACHIEVEMENTS && _achieve.achievementParentID == _parent.achievementID)
            {
                rightNavs[desiredCount].GetComponent<QFClickableAchievement>().achievement = _achieve;
                rightNavs[desiredCount].GetComponentInChildren<Text>().text = _achieve.achievementName;
                desiredCount++;
            }
        }

        if (desiredCount < NUMBER_OF_ACHIEVEMENTS - 1)
        {
            blankChildren(desiredCount);
        }
    }

    public void blankChildren(int startingIndex)
    {
        for (int i = startingIndex; i < NUMBER_OF_ACHIEVEMENTS; i++)
        {
            rightNavs[i].GetComponentInChildren<Text>().text = "";
            rightNavs[i].GetComponentInChildren<QFClickableAchievement>().achievement = null;
        }
    }

    public void blankParents(int startingIndex)
    {
        for (int i = startingIndex; i < NUMBER_OF_ACHIEVEMENTS; i++)
        {
            leftNavs[i].GetComponentInChildren<Text>().text = "";
            leftNavs[i].GetComponentInChildren<QFClickableAchievement>().achievement = null;
        }
    }

    public void displayOnlyThisParent(AchievementItem _parent)
    {
        achievements = gameObject.GetComponent<QFMenuController>().achievements;

        currentParentPage = 1;

        foreach (AchievementItem _achieve in achievements.achievements)
        {
            if (_achieve == _parent)
            {
                leftNavs[0].GetComponent<QFClickableAchievement>().achievement = _achieve;
                leftNavs[0].GetComponentInChildren<Text>().text = _achieve.achievementName;
            }
        }

        blankParents(1);
    }

    public void displayAllTopLevelAchievements()
    {
        achievements = gameObject.GetComponent<QFMenuController>().achievements;

        currentParentPage = 1;
        int desiredCount = 0;

        foreach (AchievementItem _achieve in achievements.achievements)
        {
            if (desiredCount < NUMBER_OF_ACHIEVEMENTS && _achieve.isTopLevel)
            {
                leftNavs[desiredCount].GetComponent<QFClickableAchievement>().achievement = _achieve;
                leftNavs[desiredCount].GetComponentInChildren<Text>().text = _achieve.achievementName;
                desiredCount++;
            }
        }

        if (desiredCount < NUMBER_OF_ACHIEVEMENTS)
        {
            for (int i = desiredCount; i < NUMBER_OF_ACHIEVEMENTS; i++)
            {
                leftNavs[desiredCount].GetComponent<QFClickableAchievement>().achievement = null;
                leftNavs[desiredCount].GetComponentInChildren<Text>().text = "";
            }
        }
    }

    public Sprite getIconByID(int _id, Sprite _sprite)
    {
        iconList = gameObject.GetComponent<QFMenuController>().achievementIconList;

        foreach (IconItem _icon in iconList.iconItemList)
        {
            if (_icon.iconID == _id)
            {
                _sprite = _icon.icon;
            }
        }

        return _sprite;
    }

    public void pageTabs(bool forward)
    {
        int desiredPage = currentIconPage;

        if (forward)
        {
            desiredPage++;
        }
        else
        {
            desiredPage--;
        }
        pageTabs(desiredPage);
    }

    public void pageTabsByGUI()
    {
        int desiredPage;

        try
        {
            desiredPage = Int32.Parse(iconPickerPageNumberInput.text);
        }
        catch
        {
            desiredPage = 1;
        }

        if (desiredPage < 1)
        {
            desiredPage = 1;
        }

        pageTabs(desiredPage);
    }

    public void pageTabs(int page)
    {
        int iconCount = iconList.iconItemList.Count;

        int totalPageCount = iconCount / NUMBER_OF_ACHV_ICONS;

        if (totalPageCount <= 0)
        {
            totalPageCount = 1;
        }

        int targetPage = 1;

        if (page > totalPageCount)
        {
            targetPage = totalPageCount;
            indexOficons = iconCount - NUMBER_OF_ACHV_ICONS;
        }
        else if (page < 1)
        {
            targetPage = 1;
            indexOficons = 0;
        }
        else
        {
            targetPage = page;
            indexOficons = targetPage * NUMBER_OF_ACHV_ICONS - NUMBER_OF_ACHV_ICONS;
        }

        currentIconPage = targetPage;
        iconsChanged();
    }

    public void iconsChanged()
    {
        iconList = gameObject.GetComponent<QFMenuController>().achievementIconList;

        int iconCount = 0;

        if (iconList.iconItemList.Count < NUMBER_OF_ACHV_ICONS)
        {
            iconCount = iconList.iconItemList.Count;
        }
        else
        {
            iconCount = NUMBER_OF_ACHV_ICONS;
        }

        int x = 0;
        int indexcount = 0;

        foreach (IconItem _icon in iconList.iconItemList)
        {
            if (indexcount >= indexOficons)
            {
                if (x < iconCount)
                {
                    achievementIcons[x].sprite = getIconByID(_icon.iconID, achievementIcons[x].sprite);
                    achievementIcons[x].GetComponent<QFClickableIcon>().iconNumber = _icon.iconID;
                }
                if (x >= iconCount && x < NUMBER_OF_ACHV_ICONS)
                {
                    achievementIcons[x].GetComponent<QFClickableIcon>().iconNumber = 0;
                }
                x++;
            }

            indexcount++;
        }
    }

    public void iconSelected(int iconNumber)
    {
        tempAchievement.achievementIconID = iconNumber;
        currentAchievementIcon.sprite = getIconByID(iconNumber, currentAchievementIcon.sprite);
    }

    public void setParentAchievementByDropDown()
    {
        tempAchievement.achievementParentID = QFAchievementHelper.achievementNameToID(achievementParentDropdown.captionText.text, achievements);
        tempAchievement.isTopLevel = false;
        displayAllTopLevelAchievements();

        achievementSelected(QFAchievementHelper.getAchievementByID(tempAchievement.achievementParentID, achievements), true);
    }

    public void initParentAchievementDropDown(Dropdown _dropdown)
    {
        bool firstset = false;
        _dropdown.options.Clear();

        int count = 0;
        foreach (AchievementItem _achieve in achievements.achievements)
        {
            count++;
            Dropdown.OptionData item = new Dropdown.OptionData();
            item.text = _achieve.achievementName;

            if (!firstset)
            {
                _dropdown.captionText.text = _achieve.achievementName;
                firstset = true;
            }
            _dropdown.options.Add(item);
        }

        if (count == 0)
        {
            _dropdown.captionText.text = "NONE FOUND";
        }
    }

    public void achievementSelected(AchievementItem _achievement)
    {
        tempAchievement = _achievement;
        initSelectedAchievement();

        AchievementItem _achiev = achievements.achievements.Where(x => x.achievementID == _achievement.achievementID).SingleOrDefault();

        tempAchievement = _achiev;

        displayChildrenOf(tempAchievement);

        if (tempAchievement != null)
        {
            initSelectedAchievement();
        }
    }

    public void achievementIconsChanged()
    {
        iconList = gameObject.GetComponent<QFMenuController>().achievementIconList;

        int iconCount = 0;

        if (iconList.iconItemList.Count < NUMBER_OF_ACHV_ICONS)
        {
            iconCount = iconList.iconItemList.Count;
        }
        else
        {
            iconCount = NUMBER_OF_ACHV_ICONS;
        }

        int x = 0;
        int indexcount = 0;

        foreach (IconItem _icon in iconList.iconItemList)
        {
            if (indexcount >= indexOficons)
            {
                if (x < iconCount)
                {
                    achievementIcons[x].sprite = getIconByID(_icon.iconID, achievementIcons[x].sprite);
                    achievementIcons[x].GetComponent<QFClickableIcon>().iconNumber = _icon.iconID;
                }
                if (x >= iconCount && x < NUMBER_OF_ACHV_ICONS)
                {
                    achievementIcons[x].GetComponent<QFClickableIcon>().iconNumber = 0;
                }
                x++;
            }

            indexcount++;
        }
    }

    private void initScrollPanels()
    {
        leftNavs = new GameObject[NUMBER_OF_ACHIEVEMENTS];
        rightNavs = new GameObject[NUMBER_OF_ACHIEVEMENTS];

        achievementIcons[0] = achievementIcon;
        leftNavs[0] = leftNavAchievItem;
        leftNavs[0].GetComponentInChildren<Text>().text = "";
        leftNavs[0].GetComponentInChildren<QFClickableAchievement>().achievement = null;

        rightNavs[0] = rightNavAchievItem;
        rightNavs[0].GetComponentInChildren<Text>().text = "";
        rightNavs[0].GetComponentInChildren<QFClickableAchievement>().achievement = null;

        for (int x = 1; x < NUMBER_OF_ACHV_ICONS; x++)
        {
            Image newButton = (Image)Instantiate(achievementIcon, transform.position, transform.rotation);
            newButton.transform.SetParent(achievementIconPanel.transform, true);
            achievementIcons[x] = newButton.transform.GetComponent<Image>();
            newButton.name = "achImage" + "0" + (x);
        }

        for (int i = 1; i < NUMBER_OF_ACHIEVEMENTS; i++)
        {
            GameObject lNavGo = Instantiate(leftNavAchievItem);
            lNavGo.transform.SetParent(leftNavPanel.transform);
            leftNavs[i] = lNavGo;
            leftNavs[i].GetComponentInChildren<Text>().text = "";
            leftNavs[i].GetComponentInChildren<QFClickableAchievement>().achievement = null;

            GameObject rNavGo = Instantiate(rightNavAchievItem);
            rNavGo.transform.SetParent(rightNavPanel.transform);
            rightNavs[i] = rNavGo;
            rightNavs[i].GetComponentInChildren<Text>().text = "";
            rightNavs[i].GetComponentInChildren<QFClickableAchievement>().achievement = null;
        }
    }
}

/*
  public GameObject leftNavAchievItem;
    public GameObject rightNavAchievItem;
    public GameObject[] leftNavs;
    public GameObject[] rightNavs;
    public GameObject leftNavPanel;
    public GameObject rightNavPanel;
    */