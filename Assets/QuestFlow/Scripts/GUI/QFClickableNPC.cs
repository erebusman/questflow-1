﻿using QuestFlow;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/*
 * Copyright 2015 Carl Kidwell
 *
 * QuestFlow Quest and Dialogue editor for Unity 3D
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class QFClickableNPC : MonoBehaviour, IPointerDownHandler
{
    public int iconNumber;
    public NPCItem npc;
    public Image iconImage;
    public bool selected = false;
    public bool iconPickerActive = false;

    private QFNPCController qfNPCController;

    public Color normalColor = Color.white;
    public Color dimColor = Color.gray;

    // Use this for initialization
    private void Start()
    {
        qfNPCController = FindObjectOfType(typeof(QFNPCController)) as QFNPCController;
        updateSelectedColor();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left || eventData.button == PointerEventData.InputButton.Right)
        {
            if (!iconPickerActive)
            {
                if (npc == null)
                {
                    return;
                }

                qfNPCController.npcSelected(npc);
                selected = true;
                updateSelectedColor();
            }
            else
            {
                qfNPCController.assignIcon(iconNumber);
                selected = true;
                updateSelectedColor();
            }
        }
    }

    public void updateSelectedColor()
    {
        if (selected)
        {
            iconImage.color = normalColor;
        }
        else
        {
            iconImage.color = dimColor;
        }
    }
}