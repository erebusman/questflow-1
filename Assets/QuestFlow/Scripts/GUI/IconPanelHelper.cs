﻿using QuestFlow;
using UnityEngine;
using UnityEngine.UI;

public class IconPanelHelper : MonoBehaviour
{
    private static int NUMBER_OF_ICONS = 35;

    public Image iconImage;
    public Image[] icons = new Image[NUMBER_OF_ICONS];
    public GameObject iconContainerPanel;
    public GameObject iconPickerPanel;
    public bool firstTimeInitDone = false;
    public int indexOficons = 0;

    public IconList iconList;
    private int currentIconPage = 1;
    private int currentNPCIconPage = 1;
    private int desiredIconPage = 1;

    private IconEvent iconEvent = new IconEvent();

    private QFMenuController qfMenuController;

    private void Start()
    {
        qfMenuController = gameObject.GetComponent<QFMenuController>();
        iconList = qfMenuController.achievementIconList;
        initIconPanel();
        iconListChanged();
        iconEvent.iconType = IconEvent.IconType.NONE;
    }

    public void onPanelLoaded()
    {
        if (!firstTimeInitDone)
        {
            iconList = qfMenuController.npcIconList;
            initIconPanel();
            firstTimeInitDone = true;
        }
    }

    public void loadNPCIcons()
    {
        desiredIconPage = 1;
        iconPickerPanel.SetActive(true);
        iconList = qfMenuController.npcIconList;
        iconEvent.iconType = IconEvent.IconType.NPC;
        clearCurrentList();
        iconListChanged();
    }

    public void loadAchievementIcons()
    {
        desiredIconPage = 1;
        iconPickerPanel.SetActive(true);
        onPanelLoaded();
        iconList = qfMenuController.achievementIconList;
        iconEvent.iconType = IconEvent.IconType.ACHIEVEMENT;
        iconListChanged();
    }

    public void loadLocationIcons()
    {
        desiredIconPage = 1;
        iconPickerPanel.SetActive(true);
        onPanelLoaded();
        iconList = qfMenuController.backgroundIconList;
        iconEvent.iconType = IconEvent.IconType.LOCATION;
        iconListChanged();
    }

    public void pageTabs(bool forward)
    {
        int desiredPage;

        desiredPage = currentIconPage;

        if (forward)
        {
            desiredPage++;
        }
        else
        {
            desiredPage--;
        }
        pageTabs(desiredPage);
    }

    public void pageTabsByGUI()
    {
        int desiredPage = desiredIconPage;

        if (desiredPage < 1)
        {
            desiredPage = 1;
        }

        pageTabs(desiredPage);
    }

    public void pageTabs(int page)
    {
        int iconCount;

        iconCount = iconList.iconItemList.Count;

        int totalPageCount = iconCount / NUMBER_OF_ICONS;

        if (totalPageCount <= 0)
        {
            totalPageCount = 1;
        }

        int targetPage = 1;

        if (page > totalPageCount)
        {
            targetPage = totalPageCount;
            indexOficons = iconCount - NUMBER_OF_ICONS;
        }
        else if (page < 1)
        {
            targetPage = 1;
            indexOficons = 0;
        }
        else
        {
            targetPage = page;
            indexOficons = targetPage * NUMBER_OF_ICONS - NUMBER_OF_ICONS;
        }

        currentIconPage = targetPage;
        iconListChanged();
    }

    public void iconListChanged()
    {
        //iconList = gameObject.GetComponent<QFMenuController>().npcIconList;

        int iconCount = 0;
        if (iconList.iconItemList.Count < NUMBER_OF_ICONS)
        {
            iconCount = iconList.iconItemList.Count;
        }
        else
        {
            iconCount = NUMBER_OF_ICONS;
        }
        int x = 0;
        int indexcount = 0;

        foreach (IconItem item in iconList.iconItemList)
        {
            if (indexcount >= indexOficons)
            {
                if (x < iconCount)
                {
                    icons[x].sprite = item.icon;
                    icons[x].GetComponent<QFClickableIcon>().iconNumber = item.iconID;
                    icons[x].GetComponent<QFClickableIcon>().setIconType(iconEvent.iconType);
                    IconEvent.IconType duh = icons[x].GetComponent<QFClickableIcon>().currentIconPickerEvent.iconType;
                    int yyz = icons[x].GetComponent<QFClickableIcon>().iconNumber;
                    //icons[x].GetComponent<QFClickableIcon>().currentIconPickerEvent.iconType = iconEvent.iconType;
                }
                if (x >= iconCount && x < NUMBER_OF_ICONS)
                {
                    icons[x].sprite = getIconByID(0, icons[x].sprite);
                    icons[x].GetComponent<QFClickableIcon>().iconNumber = 0;
                    icons[x].GetComponent<QFClickableIcon>().setIconType(IconEvent.IconType.NONE);
                    //icons[x].GetComponent<QFClickableIcon>().currentIconPickerEvent.iconType = IconEvent.IconType.NONE;
                }
                x++;
            }

            indexcount++;
        }

        if (x < NUMBER_OF_ICONS)
        {
            for (int y = x; x < NUMBER_OF_ICONS; x++)
            {
                icons[x].sprite = getIconByID(0, icons[x].sprite);
                icons[x].GetComponent<QFClickableIcon>().iconNumber = 0;
                icons[x].GetComponent<QFClickableIcon>().currentIconPickerEvent.iconType = IconEvent.IconType.NONE;
            }
        }
    }

    public void setDesiredIconPage(int _desiredPage)
    {
        desiredIconPage = _desiredPage;
    }

    public Sprite getIconByID(int _id, Sprite _sprite)
    {
        foreach (IconItem _icon in iconList.iconItemList)
        {
            if (_icon.iconID == _id)
            {
                _sprite = _icon.icon;
            }
        }

        return _sprite;
    }

    private void clearCurrentList()
    {
        for (int x = 0; x < NUMBER_OF_ICONS; x++)
        {
            icons[x].sprite = new Sprite();
            icons[x].GetComponent<QFClickableIcon>().iconNumber = 0;
            icons[x].GetComponent<QFClickableIcon>().currentIconPickerEvent.iconType = IconEvent.IconType.NONE;
        }
    }

    private void initIconPanel()
    {
        icons[0] = iconImage;

        for (int x = 1; x < NUMBER_OF_ICONS; x++)
        {
            Image newIcon = (Image)Instantiate(iconImage, transform.position, transform.rotation);
            newIcon.transform.SetParent(iconContainerPanel.transform, true);
            icons[x] = newIcon.transform.GetComponent<Image>();
            newIcon.name = "Image" + (x);
            IconEvent.IconType duh = icons[x].GetComponent<QFClickableIcon>().currentIconPickerEvent.iconType;
            int yyz = icons[x].GetComponent<QFClickableIcon>().iconNumber;
        }
    }
}