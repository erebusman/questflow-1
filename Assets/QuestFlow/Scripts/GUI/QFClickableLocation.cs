﻿using QuestFlow;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/*
 * Copyright 2015 Carl Kidwell
 *
 * QuestFlow Quest and Dialogue editor for Unity 3D
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class QFClickableLocation : MonoBehaviour, IPointerDownHandler
{
    private QFLocationController qFLocationController;

    public LocationItem locationItem;

    public Image panelImage;

    public Color normalColor = Color.white;
    public Color dimColor = Color.gray;

    public bool selected = false;

    private void Start()
    {
        qFLocationController = FindObjectOfType(typeof(QFLocationController)) as QFLocationController;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left || eventData.button == PointerEventData.InputButton.Right)
        {
            selected = true;
            qFLocationController.locationSelected(locationItem);
            updateSelectedColor();
        }
    }

    public void updateSelectedColor()
    {
        if (selected)
        {
            panelImage.color = normalColor;
        }
        else
        {
            panelImage.color = dimColor;
        }
    }
}