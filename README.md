# README #

This README would normally document whatever steps are necessary to get your application up and running.

### QuestFlow 1 ###

QuestFlow 1 is a Quest and Dialogue system for Unity 3D 5.x line written in C#.

QuestFlow 1 is Apache 2.0 licensed

If licenses are TLDR for you: yes you can make a game with this and sell the game if you wish!

### QuestFlow 1 includes:###

* the core QuestFlow files which is a series of classes that utilize XML serialization and deserialization to save and load quests
* A Unity 3D scene with a front end (GUI) for editing for Campaign, Quests, NPC's, Conversations, Locations, and Achievements
* Unity Editor Scripts for creating the scriptable objects for Icon libraries
* Unity Editor Windows for editing the Icon library scriptable object
* an Observer pattern based Quest Event Manager script to use in your implementation of the system in game.
* some generic template icons to get you started prototyping
* a sample TestCanvas demoing the QuestEventManager

### Quest Flow 1 does not include: ###

* A gameplay implementation.

### How do I get set up? ###

#### Option 1: Import the QuestFlow1.unitypackage ####

If you just want to use the system (including modifying the code to suit your own project needs if neccesary) 
then you can just download the QuestFlow1.unitypackage to your system and then inside of Unity 3D click on the
top menu bar Assets > Import Package > Custom Package and navigate to the location on your local disk where you 
saved the package and import it.

#### Option 2: Download the source code ####

If you have an interest in contributing to Quest Flow 1 then you would download/branch the sourcecode from bitbucket.org. 
You will get all of the same content as the unitypackage mentioned above, however if you want to make your changes under
source control and potentially contribute back to the project you will be able to do so.

### Dependencies###

Unity 3D 5.x is required to run this. The code is written in C#, but there's no reason you couldn't write your game play
implementation in another language if you are more comfortable with something else (unity script etc).

### Basic Usage Instructions ###

QuestFlow 1 depends on XML serialization to save/load the quests it creates.  This permits the quests to be edited and saved at runtime.

You can create a set of quests using the provided GUI front end.

* Load up QuestFlow in Unity
* hit play in the QuestFlow scene to begin
* fill in a campaign name (mandatory)
* optional: fill in a description and min/max levels supported for this campaign
* click create new
* in the upper right click "Save"
* (BUG) currently the editor wont know about the new XML files until you stop the scene, and then press "PLAY" again
* Notice now that you have hit play again the drop down in the top right contains the name of the campaign you just created
* Click "open" below it
* Now tab between each tab and you can create new quests, conversations, npcs, achievements, and locations

Further instructions will be provided eventually but the GUI is pretty rough right now as its a first pass.

I will continue working on this over the next year; documenting it heavily right now would just mean I need to do it all over again later.


### Implementing in gameplay ###

QuestFlow 1 does not include a game play implementation; this is the back end XML for a quest system.

Because gameplay is very implementation specific that is left to you at this time. However if a contributor comes up with something
really slick I would consider merging it.


### Contribution guidelines ###

#### Content Direction####

This is a beta 1.0 product at this time; its unclear which direction it might take in the future. 
Large architectural changes might be better forked but I'm willing to discuss ideas.

### Who do I talk to? ###

* Repo owner or admin

ckidwell at infernohawke (com) is the owner of this work if you need to reach out/contact.